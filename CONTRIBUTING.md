# Contributing
Thanks for contributing to this project! Please follow the below guidelines for your development.

## Developer Guidelines

### Licensing
All code developed and released through this repository should attach the appropriate Google disclaimer on the header of source files.
```
Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose. Your use of it is subject to your agreement with Google. 
```

### Code Guidelines
* Code submitted to this repository should follow the appropriate [style guide](https://google.github.io/styleguide) for submission's language.
* Use linters or formatters to check & fix code style issues before submission:
  * Java
    * Use this checkstyle [configuration](https://github.com/checkstyle/checkstyle/blob/master/src/main/resources/google_checks.xml) to adhere to the Google style.
    * If developing Dataflow pipelines, you can utilize the [Maven Archetype Dataflow](https://github.com/GoogleCloudPlatform/professional-services/tree/master/tools/maven-archetype-dataflow) to bootstrap projects with appropriate checkstyle and spotbugs configurations. This archetype will also attach license headers automatically to source files, follow the README directions for changing the license.
  * Python
    * Use [pylint](https://www.pylint.org/) to check for style adherence.
      * Installed with `pip install pylint`.
  * Go
    * Use [Gofmt](https://blog.golang.org/go-fmt-your-code) to format your code.
    * Use [golint](https://github.com/golang/lint) to point out style mistakes.
  * Terraform
    * Use the [fmt](https://www.terraform.io/docs/commands/fmt.html) to rewrite Terraform files in the canonical format and style.
    * Use the [validate](https://www.terraform.io/docs/commands/validate.html) command to validate the syntax of terraform files.
    * Follow the [Terraform Standards Guide](https://docs.google.com/document/d/1O95awUFmDGJopgxBKqkMkazkG1nuhz0KVOontTDb6Jk/edit#)

### Step-by-Step Guide

1. Clone the repo.
`gcloud source repos clone redacted.com-foundations --project=cloud-professional-services`
1. Develop and test your code changes.
    1. Ensure that your code adheres to the existing style.
    2. Ensure that your code has an appropriate set of unit tests which all pass.
    3. Ensure all source files have license headers with an up-to-date copyright date attributed to Google LLC.
1. Submit your changes. 
    1. If using code review: `git push origin HEAD:refs/for/master`
    2. If submitting directly: `git push origin master`
