# Redacted Foundations

This repo contains Terraform code for encoding the Redacted Foundation in Google Cloud.

## Repo Structure
This repo is a monorepo containing all foundation deliverables from Google. It can be split into multiple repos within GitLab.

The directory structure is as follows:

- `modules/`: shared modules
- `foundation/`: foundational components, with a subdirectory per component:
    - `component/`: the particular component (ex. networking)
        - `prod/`: folder for each environment
            - `main.tf`
            - `backend.tf`
        - `dev/`
- `experimental/`: containers components which have not yet been adopted
- `build/`: gitlab-ci example configurations
- `policy-library/`: Config Validator policy repository
  - `policies/constraints`: example constraints for the redacted.com environment.

## Contributing
Before contributing, please review the [contributing guidelines](CONTRIBUTING.md) for submitting code to this repository.

## License
```
Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose. Your use of it is subject to your agreement with Google.
```

## Disclaimer
This is not an official Google product.

## Contact
Questions, issues, and comments should be directed to: **redacted-cloud-onboarding-external@redacted.com**.
