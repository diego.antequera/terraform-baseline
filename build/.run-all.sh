#!/bin/bash
set -e
set +x

BASE_DIR="$(pwd)/foundation"

YAML_FILE="$(pwd)/build/$1"
ENV_FILTER=$2

ls $BASE_DIR | while read component ; do
  ls $BASE_DIR/$component | while read env ; do
    if [ "$ENV_FILTER" == "$env" ] || [ -z $ENV_FILTER ] ; then
      echo "$component/$env matches $ENV_FILTER; triggering build"
      gcloud builds submit . --config $YAML_FILE --substitutions=_COMPONENT=$component,_ENV=$env
    else
      echo "$component/$env doesn't match $ENV_FILTER; skipping"
    fi
  done
done
