#!/bin/bash
set -e
set +x

# sh foundation/cloud_build/org/cloudbuild.sh validate ./foundation/projects/blk_example3 blk-dev-fmg-data-analysis dev
BASE_DIR="$( cd "$( dirname "$0" )" && pwd )"

MIN_ARGS=4
if [ "$#" -lt ${MIN_ARGS} ];
then
  echo "ERROR: Need 3 parameters: COMMAND, WORKSPACE_DIR, PROJECT_ID and ENV"
  exit -1
fi

YAML_FILE=""
case $1 in
  validate)
    YAML_FILE=cloudbuild-validate.yaml
    break
    ;;
  apply)
    YAML_FILE=cloudbuild-apply.yaml
    break
    ;;
  *)
    echo "ERROR: Command not recognized; Expected validate|apply"
    exit -1
    break
    ;;
esac

gcloud builds submit . \
  --config=$BASE_DIR/$YAML_FILE \
  --substitutions=_WORKSPACE_DIR=$2,_PROJECT_ID=$3,_ENV=$4 \
  --project=blk-prod-core-cloud-build \
  --verbosity=info