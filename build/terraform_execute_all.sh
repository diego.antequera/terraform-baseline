#!/bin/bash
ORG_FOLDER="org"
LOCAL_DIR=$(pwd)
FOUNDATION_DIR="foundation"
cd $FOUNDATION_DIR

execute ()
{
    cd "$LOCAL_DIR"
    echo "executing: gcloud builds submit . --config build/cloudbuild-$3.yaml --substitutions=_COMPONENT=$1,_ENV=$2 --project=$GCP_PROJECT"
    gcloud builds submit . --config build/cloudbuild-$3.yaml --substitutions=_COMPONENT=$1,_ENV=$2 --project=$GCP_PROJECT
    cd "$LOCAL_DIR/$FOUNDATION_DIR"
}

for COMPONENT in *
do
    if [ -d "$COMPONENT/$BRANCH_NAME/" ]; then
        execute $COMPONENT $BRANCH_NAME $1
    elif [ $BRANCH_NAME = "prod" -a -d "$dir/$ORG_FOLDER/" ]; then
        execute $COMPONENT "org" $1
    else
        cd "$LOCAL_DIR/$FOUNDATION_DIR/$COMPONENT"
        for ENV in *
        do
            if [[ $ENV = "$BRANCH_NAME"* ]]; then
                execute $COMPONENT $ENV $1
            fi
        done
    fi
done
