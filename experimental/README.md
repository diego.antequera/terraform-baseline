# Experimental Components

This directory contains experimental components which has not yet been adopted.
These components were excluded from the initial foundation build-out but are available for future reference.

To adopt one of these components, it should first be moved into the `foundation/` folder.
