# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "prod_network" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "prod_billing_export"
}

module "bigquery" {
  source  = "terraform-google-modules/bigquery/google"
  version = "~> 3.0"

  dataset_id     = "billing_exports"
  dataset_name   = "billing_exports"
  description    = "Org billing exports dataset"
  project_id     = module.prod_network.state.project_id
  location       = "US"
  dataset_labels = {}
}