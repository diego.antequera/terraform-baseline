# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "folders" {
  source    = "../../../modules/remote"
  component = "folders"
  env       = "root"
}

module "dlp_scanner_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "dev_dlp_scanner"
}

locals {
  dlp_service_account = "service-${module.dlp_scanner_project.state.project_number}@dlp-api.iam.gserviceaccount.com"
  env                 = "dev"
}

module "event_folder_log_entry" {
  # source  = "terraform-google-modules/event-function/google//modules/event-folder-log-entry"
  # version = "~> 1.2.0"
  source = "../../../modules/external/event_function/modules/event-folder-log-entry"

  filter           = "logName:activity ((resource.type=gcs_bucket protoPayload.methodName=storage.buckets.create) OR (resource.type=bigquery_dataset protoPayload.methodName=google.cloud.bigquery.v2.TableService.InsertTable))"
  name             = "${module.info.prefix}-dev-core-dlp"
  folder_id        = module.folders.state.root_dev_folders_id
  project_id       = module.dlp_scanner_project.state.project_id
  include_children = true
}

module "dlp_service_account" {
  source     = "terraform-google-modules/service-accounts/google"
  version    = "~> 2.0"
  project_id = module.dlp_scanner_project.state.project_id
  prefix     = "${module.info.prefix}-${local.env}-dlp"
  names      = ["sa"]
}

resource "google_folder_iam_binding" "object_viewer_binding" {
  folder = module.folders.state.root_dev_folders_id
  role   = "roles/storage.objectViewer"

  members = ["serviceAccount:${local.dlp_service_account}"]
}

resource "google_folder_iam_binding" "bigquery_viewer_binding" {
  folder = module.folders.state.root_dev_folders_id
  role   = "roles/bigquery.dataViewer"

  members = ["serviceAccount:${local.dlp_service_account}"]
}

resource "google_project_iam_binding" "cloudfunctions_serviceAgent_binding" {
  project = module.dlp_scanner_project.state.project_id
  role    = "roles/cloudfunctions.serviceAgent"

  members = module.dlp_service_account.iam_emails_list
}

resource "google_project_iam_binding" "dlp_admin_binding" {
  project = module.dlp_scanner_project.state.project_id
  role    = "roles/dlp.admin"

  members = module.dlp_service_account.iam_emails_list
}

resource "google_project_iam_binding" "logging_logWriter_binding" {
  project = module.dlp_scanner_project.state.project_id
  role    = "roles/logging.logWriter"

  members = module.dlp_service_account.iam_emails_list
}

module "pubsub_destination" {
  source  = "terraform-google-modules/pubsub/google"
  version = "~> 1.0"

  topic      = "${module.info.prefix}-${local.env}-core-dlp"
  project_id = module.dlp_scanner_project.state.project_id
}

module "dlp_function" {
  # source = "terraform-google-modules/event-function/google"
  # version = "~> 1.2.0"
  source = "../../../modules/external/event_function"

  description = "DLP Scan Function"
  entry_point = "ReceiveMessage"
  runtime     = "go111"
  timeout_s   = "240"

  event_trigger      = module.event_folder_log_entry.function_event_trigger
  name               = "${module.info.prefix}-${local.env}-core-dlp-scan_us-a"
  project_id         = module.dlp_scanner_project.state.project_id
  region             = module.info.regions.us_a
  source_directory   = "../org/function_source/"
  bucket_policy_only = true
  bucket_name        = "${module.info.prefix}-${local.env}-dlp-scan-bucket"

  environment_variables = {
    CURRENT_ENV = local.env
    PROJECT_ID  = module.dlp_scanner_project.state.project_id
    LOCATION_ID = module.info.regions.us_a
    TOPIC_NAME  = module.pubsub_destination.topic
  }

  service_account_email = module.dlp_service_account.email
}
