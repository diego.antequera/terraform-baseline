package dlpScanner

import (
	"context"
	"fmt"
	"encoding/json"
	"os"
	"strings"

	"cloud.google.com/go/pubsub"
	dlp "cloud.google.com/go/dlp/apiv2"
	"github.com/golang/protobuf/ptypes/duration"
	dlppb "google.golang.org/genproto/googleapis/privacy/dlp/v2"
)

type creationEvent struct {
	JsonPayload struct {
		Actor struct {
			UserID			string `json:"principalEmail"`
		} `json:"authenticationInfo"`
		TableID				string `json:"resourceName"`
	} `json:"protoPayload"`
	Resource struct {
		Type 					string `json:"type"`
		Labels struct {
			BucketName 	string `json:"bucket_name,omitempty"`
			DatasetID 	string `json:"dataset_id,omitempty"`
			ProjectID  	string `json:"project_id"`
		} `json:"labels"`
	} `json:"resource"`
}

func ReceiveMessage(ctx context.Context, msg *pubsub.Message) error {
	var event creationEvent
	fmt.Printf("Message received. %v\n", msg)
	json.Unmarshal([]byte(msg.Data), &event)

	fmt.Printf("Create DLP Scanning Job and Trigger. %v\n", event)

	client, err := dlp.NewClient(ctx)
	if err != nil {
		return fmt.Errorf("dlp.NewClient: %v", err)
	}
	fmt.Printf("DLP Client created. %v\n", client)

	var infoTypes []*dlppb.InfoType
	for _, it := range getDefaultInfoTypes() {
		infoTypes = append(infoTypes, &dlppb.InfoType{Name: it})
	}
	fmt.Printf("InfoTypes to be scanned: %v\n", infoTypes)

	destinationProjectID := os.Getenv("PROJECT_ID")
	sourceProjectID := event.Resource.Labels.ProjectID
	// locationID := os.Getenv("LOCATION_ID")
	description := "Cloud Function to create a DLP Scanner for this bucket/bigquery"

	var storageConfigInfo *dlppb.StorageConfig
	var storageName string
	var triggerID string
	fmt.Printf("Resource type is %v\n", event.Resource.Type)

	if event.Resource.Type == "gcs_bucket" {
		storageName = event.Resource.Labels.BucketName
		storageConfigInfo = getBucketStorageConfig(storageName)
		triggerID = storageName + "-dlp-trigger"
	} else if event.Resource.Type == "bigquery_dataset" {
		storageName = event.Resource.Labels.DatasetID
		temp := strings.Split(event.JsonPayload.TableID, "/")
		tableID := temp[len(temp) - 1]
		triggerID = storageName + "-" + tableID + "-dlp-trigger"
		storageConfigInfo = getBigQueryStorageConfig(storageName, tableID, sourceProjectID)
	}

	displayName := "DLP Job Trigger for " + event.Resource.Type + ":" + storageName
	fmt.Printf("Storage name: %v\n", storageName)

	topicName := "projects/" + sourceProjectID + "/topics/" + os.Getenv("TOPIC_NAME")

	fmt.Printf("Preparing request for trigger '%v'\n", triggerID)
	req := &dlppb.CreateJobTriggerRequest{
		Parent:    "projects/" + destinationProjectID,
		TriggerId: triggerID,
		JobTrigger: &dlppb.JobTrigger{
			DisplayName: displayName,
			Description: description,
			Status:      dlppb.JobTrigger_HEALTHY,
			Triggers: []*dlppb.JobTrigger_Trigger{
				{
					Trigger: &dlppb.JobTrigger_Trigger_Schedule{
						Schedule: &dlppb.Schedule{
							Option: &dlppb.Schedule_RecurrencePeriodDuration{
								RecurrencePeriodDuration: &duration.Duration{
									Seconds: 15 * 60 * 60 * 24, // 15 days in seconds.
								},
							},
						},
					},
				},
			},
			Job: &dlppb.JobTrigger_InspectJob{
				InspectJob: &dlppb.InspectJobConfig{
					InspectConfig: &dlppb.InspectConfig{
						InfoTypes:     infoTypes,
						MinLikelihood: dlppb.Likelihood_POSSIBLE,
						Limits: &dlppb.InspectConfig_FindingLimits{
							MaxFindingsPerRequest: 10,
						},
					},
					StorageConfig: storageConfigInfo,
					Actions: []*dlppb.Action{
						{
							Action: &dlppb.Action_PubSub{
								PubSub: &dlppb.Action_PublishToPubSub{
									Topic: topicName,

								},
							},
						},
						{
							Action: &dlppb.Action_JobNotificationEmails_{
								JobNotificationEmails: &dlppb.Action_JobNotificationEmails{

								},
							},
						},
						{
							Action: &dlppb.Action_PublishSummaryToCscc_{
								PublishSummaryToCscc: &dlppb.Action_PublishSummaryToCscc{

								},
							},
						},
					},
				},
			},
		},
	}
	fmt.Printf("Object created! Calling API with '%v'\n", req)
	resp, err := client.CreateJobTrigger(ctx, req)
	if err != nil {
		return fmt.Errorf("CreateJobTrigger: %v", err)
	}
	fmt.Printf("Successfully created trigger: %v", resp.GetName())
	return nil
}

func getBucketStorageConfig(bucketName string) *dlppb.StorageConfig {
	return &dlppb.StorageConfig{
		Type: &dlppb.StorageConfig_CloudStorageOptions{
			CloudStorageOptions: &dlppb.CloudStorageOptions{
				FileSet: &dlppb.CloudStorageOptions_FileSet{
					Url: "gs://" + bucketName + "/**",
				},
			},
		},
		TimespanConfig: &dlppb.StorageConfig_TimespanConfig{
			EnableAutoPopulationOfTimespanConfig: true,
		},
	}
}

func getBigQueryStorageConfig(datasetID string, tableID string, projectID string) *dlppb.StorageConfig {
	return &dlppb.StorageConfig{
		Type: &dlppb.StorageConfig_BigQueryOptions{
			BigQueryOptions: &dlppb.BigQueryOptions{
				TableReference: &dlppb.BigQueryTable{
					ProjectId: projectID,
					DatasetId: datasetID,
					TableId:   tableID,
				},
			},
		},
	}
}

func getDefaultInfoTypes() []string {
	defaultInfoTypes := []string{
		"CREDIT_CARD_NUMBER",
		"EMAIL_ADDRESS",
		"GCP_CREDENTIALS",
		"IMEI_HARDWARE_ID",
		"IP_ADDRESS",
		"MAC_ADDRESS",
		"MAC_ADDRESS_LOCAL",
		"PASSPORT",
		"PHONE_NUMBER",
		"US_BANK_ROUTING_MICR",
		"US_EMPLOYER_IDENTIFICATION_NUMBER",
		"US_INDIVIDUAL_TAXPAYER_IDENTIFICATION_NUMBER",
		"US_SOCIAL_SECURITY_NUMBER",
		"US_VEHICLE_IDENTIFICATION_NUMBER"}
	return defaultInfoTypes
}