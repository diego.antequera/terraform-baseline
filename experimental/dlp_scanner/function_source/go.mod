module dlp-scanner

go 1.11

require (
	cloud.google.com/go v0.52.0
	cloud.google.com/go/pubsub v1.0.1
	github.com/golang/protobuf v1.3.3
	golang.org/x/lint v0.0.0-20200130185559-910be7a94367 // indirect
	golang.org/x/mod v0.2.0 // indirect
	golang.org/x/tools v0.0.0-20200211183705-e2a38c836335 // indirect
	google.golang.org/api v0.16.0
	google.golang.org/genproto v0.0.0-20200211111953-2dc5924e3898
)
