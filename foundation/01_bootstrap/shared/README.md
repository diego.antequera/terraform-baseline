# Bootstrap

This module bootstraps the GCP organization to start working with the
Cloud Foundation Toolkit.

## Usage

To bootstrap a *new* organization using this module:

1. Delete backend.tf:
   ```sh
   rm backend.tf
   ```
1. Run the following command to initialize the working directory:
   ```sh
   terraform init
   ```
1. Run the following command to review and approve the deployment:
   ```sh
   terraform apply
   ```
1. Run the following command and note the bucket key:
   ```sh
   terraform output bucket
   ```
1. Enable a remote backend by modifying `backend.tf` to look like the
   following code:
   ```hcl
   terraform {
     backend "gcs" {
       bucket = "<bucket_key>"
       prefix = "bootstrap"
     }
   }
   ```
1. Run the following command and respond "yes" to the question of
   copying the existing state to the new backend:
   ```sh
   terraform init
   ```
1. Run the following command to verify that the state has been copied
   to the GCS bucket and no resources changes are needed:
   ```sh
   terraform plan
   ```

### Permissions

The following permissions are required by the account which applies
this Terraform module.

#### Organization

- Organization Admin (`roles/resourcemanager.organizationAdmin`)
- Project Creator (`roles/resourcemanager.projectCreator`)

#### Billing Account

- Billing Admin (`roles/billing.admin`)

