# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "bootstrap" {
  source  = "terraform-google-modules/bootstrap/google"
  version = "~> 1.0.0"

  billing_account      = module.info.billing_account
  group_billing_admins = module.info.iam_groups["billing_admin"][0]

  org_id = module.info.org_id

  group_org_admins = module.info.iam_groups["org_admin"][0]
  org_admins_org_iam_permissions = [
    "roles/billing.admin",
    "roles/billing.projectManager",
    "roles/billing.user",
    "roles/compute.networkAdmin",
    "roles/compute.xpnAdmin",
    "roles/iam.securityAdmin",
    "roles/iam.serviceAccountAdmin",
    "roles/iam.organizationRoleAdmin",
    "roles/serviceusage.serviceUsageAdmin",
    "roles/logging.configWriter",
    "roles/resourcemanager.folderAdmin",
    "roles/resourcemanager.organizationViewer",
    "roles/resourcemanager.projectCreator",
    "roles/resourcemanager.projectIamAdmin",
    "roles/serviceusage.serviceUsageConsumer",
    "roles/orgpolicy.policyAdmin",
    "roles/editor",
    "roles/viewer"
  ]

  sa_org_iam_permissions = [
    # allow creating budget alerts
    "roles/billing.admin",

    # defaults
    "roles/compute.networkAdmin",
    "roles/compute.xpnAdmin",
    "roles/iam.securityAdmin",
    "roles/iam.serviceAccountAdmin",
    "roles/logging.configWriter",
    "roles/orgpolicy.policyAdmin",
    "roles/resourcemanager.folderCreator",
    "roles/resourcemanager.folderViewer",
    "roles/resourcemanager.organizationViewer",
    "roles/iam.serviceAccountKeyAdmin",
    "roles/serviceusage.serviceUsageAdmin",
    "roles/appengine.appAdmin",
    "roles/cloudfunctions.admin",
    "roles/cloudscheduler.admin",
    "roles/pubsub.admin",
    "roles/storage.admin",
    "roles/compute.securityAdmin",
    "roles/compute.instanceAdmin",
    "roles/secretmanager.secretAccessor",
    "roles/dns.admin",
  ]

  activate_apis = [
    "servicenetworking.googleapis.com",
    "compute.googleapis.com",
    "logging.googleapis.com",
    "bigquery.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "cloudbilling.googleapis.com",
    "iam.googleapis.com",
    "admin.googleapis.com",
    "appengine.googleapis.com",
    "storage-api.googleapis.com",
    "sqladmin.googleapis.com",
    "pubsub.googleapis.com",
    "cloudfunctions.googleapis.com",
    "cloudbuild.googleapis.com",
    "billingbudgets.googleapis.com",
    # required by cloudfunctions CFT
    "cloudkms.googleapis.com"
  ]
  default_region = "us-east4" # hard-coded

  project_labels = {}

  project_prefix          = "${module.info.prefix}-shared-core"
  sa_enable_impersonation = true
}
