# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "bucket" {
  description = "The state bucket."
  value       = module.bootstrap.gcs_bucket_tfstate
}

output "seed_project_id" {
  description = "Seed project Id"
  value       = module.bootstrap.seed_project_id
}

output "terraform_sa_email" {
  description = "Terraform Service Account Email"
  value       = module.bootstrap.terraform_sa_email
}

output "terraform_sa_name" {
  description = "Terraform Service Account Name"
  value       = module.bootstrap.terraform_sa_name
}
