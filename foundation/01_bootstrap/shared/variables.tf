# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

variable "billing_admins_email" {
  description = "The email address for the billing administrators."
  type        = string
  # TODO: change to BLK
  default = "project-team@redacted.com"
}

variable "region" {
  description = "The region in which resources will be created."
  type        = string
  default     = "us-east4"
}
