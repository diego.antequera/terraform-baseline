# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "folders" {
  source    = "../../../modules/remote"
  component = "folders"
  env       = "root"
}

module "core_folders" {
  source  = "terraform-google-modules/folders/google"
  version = "~> 2.0"

  parent = module.folders.state.core_folder_id
  names = concat(module.info.environments, ["shared"])
  set_roles = false
}
