# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "core_sb_folder_id" {
  description = "Id of the folder core/sb"
  value       = module.core_folders.ids["sb"]
}

output "core_dev_folder_id" {
  description = "Id of the folder core/dev"
  value       = module.core_folders.ids["dev"]
}

output "core_tst_folder_id" {
  description = "Id of the folder core/tst"
  value       = module.core_folders.ids["tst"]
}

output "core_prod_folder_id" {
  description = "Id of the folder core/prod"
  value       = module.core_folders.ids["prod"]
}

output "folder_ids" {
  description = "Id of all the core folders"
  value       = module.core_folders.ids
}
