# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "root" {
  source  = "terraform-google-modules/folders/google"
  version = "~> 2.0"

  parent  = "organizations/${module.info.org_id}"

  names = ["redacted"]
}

module "legacy" {
  source  = "terraform-google-modules/folders/google"
  version = "~> 2.0"

  parent  = "organizations/${module.info.org_id}"

  names = ["legacy"]
}

module "core" {
  source  = "terraform-google-modules/folders/google"
  version = "~> 2.0"

  parent  = module.root.id

  names = ["core"]

  set_roles = false
}

module "folders" {
  source  = "terraform-google-modules/folders/google"
  version = "~> 2.0"

  parent  = module.root.id

  names = module.info.environments

  set_roles = false
}
