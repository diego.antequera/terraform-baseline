# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "redacted_folder_id" {
  description = "Id of the folder redacted"
  value       = module.root.id
}

output "core_folder_id" {
  description = "Id of the folder core"
  value       = module.core.id
}

output "root_prod_folders_id" {
  description = "Id of the folder sroot/prod"
  value       = module.folders.ids["prod"]
}

output "root_tst_folders_id" {
  description = "Id of the folder sroot/tst"
  value       = module.folders.ids["tst"]
}

output "root_dev_folders_id" {
  description = "Id of the folder sroot/dev"
  value       = module.folders.ids["dev"]
}

output "root_sb_folders_id" {
  description = "Id of the folder sroot/sb"
  value       = module.folders.ids["sb"]
}

