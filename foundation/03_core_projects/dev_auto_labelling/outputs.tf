# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "project_id" {
  description = "Id of the Auto Labbeling project"
  value       = module.auto_labelling.project_id
}

output "project_number" {
  description = "Number of the Auto Labbeling project"
  value       = module.auto_labelling.project_number
}

