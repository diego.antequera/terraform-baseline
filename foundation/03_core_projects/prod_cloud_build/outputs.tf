# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "project_id" {
  description = "Id of the Cloud Build project"
  value       = module.cloud_build.project_id
}

output "project_number" {
  description = "Number of the Cloud Build project"
  value       = module.cloud_build.project_number
}

output "service_account_email" {
  description = "Email of the service account created for the project"
  value       = module.cloud_build.service_account_email
}

output "service_account_id" {
  description = "Id of the service account created for the project"
  value       = module.cloud_build.service_account_id
}
