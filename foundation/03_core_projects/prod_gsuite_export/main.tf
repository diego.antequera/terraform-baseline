# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

locals {
  group = module.info.iam_groups["org_admin"][0]
}

module "gsuite_export" {
  source = "../../../modules/blk_project_factory/modules/blk_project"

  resource_owner = local.group
  admin_group    = local.group

  core            = true
  business_unit   = "core"
  environment     = "prod"
  cost_center     = module.info.cost_centers["cloud"]
  detail          = "gsuite-export"
  application_id  = "core"
  data_class      = "core"
  additional_apis = ["appengine.googleapis.com"]
}

resource "google_project_service" "project_services" {
  project = module.gsuite_export.project_id
  service = "appengine.googleapis.com"
}
