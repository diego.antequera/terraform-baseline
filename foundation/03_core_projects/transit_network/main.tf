# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "folders" {
  source = "../../../modules/remote"

  component = "folders"
  env       = "core"
}

module "shared_vcp_project" {
  source  = "terraform-google-modules/project-factory/google"
  version = "~> 6.0"

  name                    = "${module.info.prefix}-transit-network"
  org_id                  = module.info.org_id
  folder_id               = module.folders.state.core_prod_folder_id
  billing_account         = module.info.billing_account
  random_project_id       = false
  default_service_account = "disable"
  auto_create_network     = true
  activate_apis           = module.info.project_activate_apis
}
