# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

locals {
  group = module.info.iam_groups["network_admin"][0]
}

module "shared_vcp_project" {
  source = "../../../modules/blk_project_factory/modules/blk_project"

  resource_owner = local.group
  admin_group    = local.group

  core           = true
  business_unit  = "core"
  environment    = "tst"
  cost_center    = module.info.cost_centers["cloud"]
  detail         = "network"
  application_id = "core"
  data_class     = "core"
}
