# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

locals {
  group = module.info.iam_groups["security_admin"][0]
}

module "project" {
  source = "../../../modules/blk_project_factory/modules/blk_project"

  resource_owner = local.group
  admin_group    = local.group

  core           = true
  business_unit  = "security"
  environment    = "tst"
  cost_center    = module.info.cost_centers["cloud"]
  detail         = "central"
  application_id = "security"
  data_class     = "security"
}
