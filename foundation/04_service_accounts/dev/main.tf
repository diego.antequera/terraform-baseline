# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "gsuite_export_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "dev_gsuite_export"
}

module "gsuite_export_service_account" {
  source        = "terraform-google-modules/service-accounts/google"
  version       = "~> 2.0"
  project_id    = module.gsuite_export_project.state.project_id
  prefix        = "${module.info.prefix}-dev"
  names         = ["gsuite-export-cf"]
  generate_keys = true
}
