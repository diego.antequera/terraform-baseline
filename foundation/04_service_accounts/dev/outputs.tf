
output "gsuite_export_service_accounts" {
  description = "GSuite Export Service Account"
  value       = module.gsuite_export_service_account.iam_emails_list
}

output "gsuite_export_service_accounts_emails" {
  description = "GSuite Export Service Account Emails"
  value       = module.gsuite_export_service_account.emails_list
}

