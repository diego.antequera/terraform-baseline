# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "folders" {
  source    = "../../../modules/remote"
  component = "folders"
  env       = "root"
}

module "dome9_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_dome9"
}

module "cloudbuild_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_cloud_build"
}

module "org_cloudbuild_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_org_cloud_build"
}

module "gsuite_export_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_gsuite_export"
}

data "terraform_remote_state" "bootstrap" {
  backend = "gcs"
  config = {
    bucket = module.info.tfstate_bucket
    prefix = "bootstrap"
  }
}

module "dome9_service_account" {
  source        = "terraform-google-modules/service-accounts/google"
  version       = "~> 2.0"
  project_id    = module.dome9_project.state.project_id
  prefix        = "${module.info.prefix}-gcp"
  names         = ["dome9"]
  generate_keys = true
}

module "gitlab_service_account" {
  source        = "terraform-google-modules/service-accounts/google"
  version       = "~> 2.0"
  project_id    = module.cloudbuild_project.state.project_id
  prefix        = "${module.info.prefix}-prod-core"
  names         = ["gitlab"]
  generate_keys = true
}

module "gitlab_org_service_account" {
  source        = "terraform-google-modules/service-accounts/google"
  version       = "~> 2.0"
  project_id    = module.org_cloudbuild_project.state.project_id
  prefix        = "${module.info.prefix}-prod-core"
  names         = ["org-gitlab"]
  generate_keys = true
}

module "gsuite_export_service_account" {
  source        = "terraform-google-modules/service-accounts/google"
  version       = "~> 2.0"
  project_id    = module.gsuite_export_project.state.project_id
  prefix        = "${module.info.prefix}-prod"
  names         = ["gsuite-export-cf"]
  generate_keys = true
}
