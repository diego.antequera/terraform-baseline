
output "dome9_service_accounts" {
  description = "Dome9 Service Account"
  value       = module.dome9_service_account.iam_emails_list
}

output "dome9_key" {
  description = "Dome9 Service Account"
  value       = jsondecode(module.dome9_service_account.keys.dome9)
}

output "cloudbuild_service_accounts" {
  description = "Cloud Build Service Account"
  value       = module.gitlab_service_account.iam_emails_list
}

output "org_cloudbuild_service_accounts" {
  description = "Org Cloud Build Service Account"
  value       = module.gitlab_org_service_account.iam_emails_list
}

output "gsuite_export_service_accounts" {
  description = "GSuite Export Service Account"
  value       = module.gsuite_export_service_account.iam_emails_list
}

output "gsuite_export_service_accounts_emails" {
  description = "GSuite Export Service Account Emails"
  value       = module.gsuite_export_service_account.emails_list
}

