# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "folders" {
  source    = "../../../modules/remote"
  component = "folders"
  env       = "root"
}

module "auto_label_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "dev_auto_labelling"
}

module "bootstrap" {
  source    = "../../../modules/remote"
  component = "bootstrap"
  env       = ""
}

module "service_accounts" {
  source    = "../../../modules/remote"
  component = "service_accounts"
  env       = "dev"
}

module "gsuite_export_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "dev_gsuite_export"
}

module "auto_label_service_account_binding" {
  source  = "terraform-google-modules/iam/google//modules/folders_iam"
  version = "~> 5.0"

  folders = [module.folders.state.root_dev_folders_id]

  bindings = {
    for role in [
      "roles/cloudfunctions.developer",
      "roles/storage.admin",
      "roles/cloudfunctions.serviceAgent",
      "roles/logging.configWriter",
      "roles/pubsub.admin",
      "roles/iam.serviceAccountUser",
      "roles/compute.viewer",
      "roles/editor"
    ] :
    role => ["serviceAccount:${module.auto_label_project.state.project_id}@appspot.gserviceaccount.com"]
  }
}

module "gsuite_export_project_bindings" {
  source  = "terraform-google-modules/iam/google//modules/projects_iam"
  version = "~> 5.0"

  projects = [module.gsuite_export_project.state.project_id]
  mode     = "additive"

  bindings = {
    for role in [
      "roles/logging.viewer",
      "roles/logging.logWriter",
      "roles/iam.serviceAccountTokenCreator",
    ] :
    role => module.service_accounts.state.gsuite_export_service_accounts
  }
}

module "terraform_serviceaccount_gsuite_bindings" {
  source  = "terraform-google-modules/iam/google//modules/projects_iam"
  version = "~> 5.0"

  projects = [module.gsuite_export_project.state.project_id]
  mode     = "additive"

  bindings = {
    "roles/owner" = ["serviceAccount:${module.bootstrap.state.terraform_sa_email}"]
  }
}

module "gsuite_export_tf_sa_bindings" {
  source  = "terraform-google-modules/iam/google//modules/service_accounts_iam"
  version = "~> 5.0"

  service_accounts = [module.bootstrap.state.terraform_sa_email]
  project          = module.bootstrap.state.seed_project_id
  mode             = "additive"
  bindings = {
    "roles/iam.serviceAccountUser" = module.service_accounts.state.gsuite_export_service_accounts
  }
}

module "gsuite_export_sa_tf_bindings" {
  source  = "terraform-google-modules/iam/google//modules/service_accounts_iam"
  version = "~> 5.0"

  service_accounts = module.service_accounts.state.gsuite_export_service_accounts_emails
  project          = module.gsuite_export_project.state.project_id
  mode             = "additive"
  bindings = {
    "roles/iam.serviceAccountUser" = ["serviceAccount:${module.bootstrap.state.terraform_sa_email}"]
  }
}
