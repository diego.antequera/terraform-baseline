# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "folders" {
  source    = "../../../modules/remote"
  component = "folders"
  env       = "root"
}

module "service_accounts" {
  source    = "../../../modules/remote"
  component = "service_accounts"
  env       = "org"
}

module "cloud_build_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_cloud_build"
}

module "org_cloud_build_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_org_cloud_build"
}

module "gsuite_export_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_gsuite_export"
}

data "terraform_remote_state" "bootstrap" {
  backend = "gcs"
  config = {
    bucket = module.info.tfstate_bucket
    prefix = "bootstrap"
  }
}

module "org_admin_bindings" {
  source  = "terraform-google-modules/iam/google//modules/organizations_iam"
  version = "~> 5.0"

  organizations = [module.info.org_id]

  bindings = {
    for role in [
      "roles/resourcemanager.organizationAdmin",
      "roles/recommender.iamAdmin"
    ] :
    role => formatlist("group:%s", module.info.iam_groups.org_admin)
  }
}

module "security_admin_bindings" {
  source  = "terraform-google-modules/iam/google//modules/organizations_iam"
  version = "~> 5.0"

  organizations = [module.info.org_id]

  bindings = {
    for role in [
      "roles/iam.organizationRoleAdmin",
      "roles/orgpolicy.policyAdmin",
      "roles/axt.admin",
      "roles/securitycenter.admin",
      "roles/threatdetection.editor",
    "roles/accesscontextmanager.policyAdmin"] :
    role => formatlist("group:%s", module.info.iam_groups.security_admin)
  }
}

module "security_auditor_bindings" {
  source  = "terraform-google-modules/iam/google//modules/organizations_iam"
  version = "~> 5.0"

  organizations = [module.info.org_id]

  bindings = {
    for role in ["roles/iam.securityReviewer"] :
    role => formatlist("group:%s", module.info.iam_groups.security_auditor)
  }
}

module "domain_wide_perms" {
  source  = "terraform-google-modules/iam/google//modules/organizations_iam"
  version = "~> 5.0"

  organizations = [module.info.org_id]

  bindings = {
    for role in ["roles/resourcemanager.organizationViewer"] :
    role => formatlist("domain:%s", module.info.org_domain)
  }
}

module "network_admin_bindings" {
  source  = "terraform-google-modules/iam/google//modules/organizations_iam"
  version = "~> 5.0"

  organizations = [module.info.org_id]

  bindings = {
    for role in [
      "roles/compute.xpnAdmin",
      "roles/compute.networkAdmin"
    ] :
    role => formatlist("group:%s", module.info.iam_groups.network_admin)
  }
}

module "billing_account_admin_bindings" {
  source  = "terraform-google-modules/iam/google//modules/billing_accounts_iam"
  version = "~> 5.0"

  billing_account_ids = [module.info.billing_account]

  bindings = {
    for role in ["roles/billing.admin"] :
    role => formatlist("group:%s", module.info.iam_groups.billing_admin)
  }
}

module "billing_account_user_bindings" {
  source  = "terraform-google-modules/iam/google//modules/billing_accounts_iam"
  version = "~> 5.0"

  billing_account_ids = [module.info.billing_account]

  bindings = {
    for role in ["roles/billing.user"] :
    role => formatlist("group:%s", module.info.iam_groups.billing_user)
  }
}

# https://sc1.checkpoint.com/documents/CloudGuard_Dome9/Documentation/Cloud-Inventory/GCP-Permissions-used.html
module "dome9_folder_bindings" {
  source  = "terraform-google-modules/iam/google//modules/organizations_iam"
  version = "~> 5.0"

  organizations = [module.info.org_id]

  bindings = {
    for role in [
      "roles/iam.securityReviewer",
      "roles/viewer",
    ] :
    role => module.service_accounts.state.dome9_service_accounts
  }
}

module "cloud_build_tf_state_binding" {
  source  = "terraform-google-modules/iam/google//modules/projects_iam"
  version = "~> 5.0"

  projects = [data.terraform_remote_state.bootstrap.outputs.seed_project_id]

  bindings = {
    for role in [
    "roles/storage.objectAdmin"] :
    role => [
      "serviceAccount:${module.org_cloud_build_project.state.project_number}@cloudbuild.gserviceaccount.com"
    ]
  }
}

module "org_cloud_build_organization_binding" {
  source  = "terraform-google-modules/iam/google//modules/organizations_iam"
  version = "~> 5.0"

  organizations = [module.info.org_id]
  mode          = "additive"

  bindings = {
    for role in [
      "roles/billing.admin",
      "roles/billing.projectManager",
      "roles/billing.user",
      "roles/compute.networkAdmin",
      "roles/compute.xpnAdmin",
      "roles/iam.securityAdmin",
      "roles/iam.serviceAccountAdmin",
      "roles/logging.configWriter",
      "roles/resourcemanager.folderAdmin",
      "roles/resourcemanager.organizationViewer",
      "roles/resourcemanager.projectCreator",
      "roles/resourcemanager.projectIamAdmin",
      "roles/resourcemanager.lienModifier",
      "roles/serviceusage.serviceUsageConsumer",
      "roles/orgpolicy.policyAdmin",
      "roles/viewer",
      "roles/cloudfunctions.developer",
      "roles/iam.serviceAccountUser",
      "roles/storage.admin",
      "roles/pubsub.editor",
      "roles/cloudkms.admin",
      "roles/serviceusage.serviceUsageAdmin",
      "roles/iam.serviceAccountKeyAdmin",
      "roles/compute.securityAdmin",
      "roles/compute.instanceAdmin",
      "roles/secretmanager.secretAccessor",
      "roles/dns.admin",
      "roles/dns.peer",
      "roles/iam.organizationRoleAdmin"
    ] :
    role => [
      "serviceAccount:${module.org_cloud_build_project.state.project_number}@cloudbuild.gserviceaccount.com"
    ]
  }
}

module "cloud_build_folder_binding" {
  source  = "terraform-google-modules/iam/google//modules/folders_iam"
  version = "~> 5.0"

  folders = [module.folders.state.redacted_folder_id]
  mode    = "additive"

  bindings = {
    for role in [
      "roles/owner"
    ] :
    role => [
      "serviceAccount:${module.org_cloud_build_project.state.project_number}@cloudbuild.gserviceaccount.com"
    ]
  }
}

module "cloud_build_tf_storage_binding" {
  source          = "terraform-google-modules/iam/google//modules/storage_buckets_iam"
  storage_buckets = [data.terraform_remote_state.bootstrap.outputs.bucket]
  mode            = "additive"

  bindings = {
    for role in [
      "roles/storage.objectAdmin"
    ] :
    role => [
      "serviceAccount:${module.org_cloud_build_project.state.project_number}@cloudbuild.gserviceaccount.com"
    ]
  }
}

module "gitlab_service_account_storage_binding" {
  source   = "terraform-google-modules/iam/google//modules/projects_iam"
  version  = "~> 5.0"
  projects = [module.org_cloud_build_project.state.project_id]

  bindings = {
    for role in [
      "roles/cloudbuild.builds.builder",
      "roles/storage.admin",
      "roles/viewer"
    ] :
    role => module.service_accounts.state.org_cloudbuild_service_accounts
  }
}

module "cloud_build_billing_bindings" {
  source  = "terraform-google-modules/iam/google//modules/billing_accounts_iam"
  version = "~> 5.0"

  billing_account_ids = [module.info.billing_account]

  bindings = {
    for role in ["roles/billing.admin"] :
    role => [
      "serviceAccount:${module.org_cloud_build_project.state.project_number}@cloudbuild.gserviceaccount.com"
    ]
  }
}

module "cloud_build_sa_project_bindings" {
  source  = "terraform-google-modules/iam/google//modules/projects_iam"
  version = "~> 5.0"

  projects = [
    module.org_cloud_build_project.state.project_id
  ]

  bindings = {
    "roles/cloudbuild.builds.editor" = module.service_accounts.state.org_cloudbuild_service_accounts
  }
}

module "gsuite_export_project_bindings" {
  source  = "terraform-google-modules/iam/google//modules/projects_iam"
  version = "~> 5.0"

  projects = [module.gsuite_export_project.state.project_id]
  mode     = "additive"

  bindings = {
    for role in [
      "roles/logging.viewer",
      "roles/logging.logWriter",
      "roles/iam.serviceAccountTokenCreator",
    ] :
    role => module.service_accounts.state.gsuite_export_service_accounts
  }
}

module "terraform_serviceaccount_gsuite_bindings" {
  source  = "terraform-google-modules/iam/google//modules/projects_iam"
  version = "~> 5.0"

  projects = [module.gsuite_export_project.state.project_id]
  mode     = "additive"

  bindings = {
    "roles/owner" = ["serviceAccount:${data.terraform_remote_state.bootstrap.outputs.terraform_sa_email}"]
  }
}

module "gsuite_export_tf_sa_bindings" {
  source  = "terraform-google-modules/iam/google//modules/service_accounts_iam"
  version = "~> 5.0"

  service_accounts = [data.terraform_remote_state.bootstrap.outputs.terraform_sa_email]
  project          = data.terraform_remote_state.bootstrap.outputs.seed_project_id
  mode             = "additive"
  bindings = {
    "roles/iam.serviceAccountUser" = module.service_accounts.state.gsuite_export_service_accounts
  }
}

module "gsuite_export_sa_tf_bindings" {
  source  = "terraform-google-modules/iam/google//modules/service_accounts_iam"
  version = "~> 5.0"

  service_accounts = module.service_accounts.state.gsuite_export_service_accounts_emails
  project          = module.gsuite_export_project.state.project_id
  mode             = "additive"
  bindings = {
    "roles/iam.serviceAccountUser" = ["serviceAccount:${data.terraform_remote_state.bootstrap.outputs.terraform_sa_email}"]
  }
}
