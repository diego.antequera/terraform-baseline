# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "folders" {
  source    = "../../../modules/remote"
  component = "folders"
  env       = "root"
}

module "auto_label_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_auto_labelling"
}

module "auto_label_service_account_binding" {
  source  = "terraform-google-modules/iam/google//modules/folders_iam"
  version = "~> 5.0"

  folders = [module.folders.state.root_dev_folders_id]

  bindings = {
    for role in [
      "roles/cloudfunctions.developer",
      "roles/storage.admin",
      "roles/cloudfunctions.serviceAgent",
      "roles/logging.configWriter",
      "roles/pubsub.admin",
      "roles/iam.serviceAccountUser",
      "roles/compute.viewer",
      "roles/editor"
    ] :
    role => ["serviceAccount:${module.auto_label_project.state.project_id}@appspot.gserviceaccount.com"]
  }
}
