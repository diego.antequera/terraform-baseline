# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "folders" {
  source    = "../../../modules/remote"
  component = "folders"
  env       = "root"
}

module "prod_network" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "prod_network"
}

module "tst_network" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "tst_network"
}

module "dev_network" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "dev_network"
}

module "sb_network" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "sb_network"
}

module "transit_network" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "transit_network"
}

module "dev_gsuite_export" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "dev_gsuite_export"
}

module "prod_gsuite_export" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "prod_gsuite_export"
}

module "os_img_prd" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod"
}

module "os_img_dev" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "dev_os_image"
}

# module "dlp_scanner_project" {
#   source    = "../../../modules/remote"
#   component = "projects"
#   env       = "dev_dlp_scanner"
# }

module "cloud_build_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_cloud_build"
}

module "org_cloud_build_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_org_cloud_build"
}

module "dev_security_org_sink_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "dev_security_org_sink"
}

module "prod_security_org_sink_project" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_security_org_sink"
}

module "prod_security_dome9" {
  source    = "../../../modules/remote"
  component = "projects"
  env       = "prod_dome9"
}

locals {
  shared_vpc_projects = [
    "projects/${module.prod_network.state.project_id}",
    "projects/${module.tst_network.state.project_id}",
    "projects/${module.dev_network.state.project_id}",
    "projects/${module.sb_network.state.project_id}",
    "projects/${module.transit_network.state.project_id}"
  ]
  os_image_exclude_projects = [
    # module.os_img_dev.state.project_id,
    # module.dev_forseti.state.project_id,
    # module.prod_forseti.state.project_id
  ]
}

#Compute Policies
module "compute-restrict-shared-vpc-host-projects" {
  source  = "terraform-google-modules/org-policy/google"
  version = "~> 3.0"

  constraint        = "constraints/compute.restrictSharedVpcHostProjects"
  policy_type       = "list"
  policy_for        = "folder"
  folder_id         = "${module.folders.state.redacted_folder_id}"
  enforce           = null
  allow_list_length = length(local.shared_vpc_projects)
  allow             = local.shared_vpc_projects
}

# module "compute-trusted-image-projects" {
#   source  = "terraform-google-modules/org-policy/google"
#   version = "~> 3.0"

#   constraint        = "constraints/compute.trustedImageProjects"
#   policy_type       = "list"
#   policy_for        = "folder"
#   folder_id         = "${module.folders.state.redacted_folder_id}"
#   enforce           = null
#   allow_list_length = "1"
#   allow             = ["projects/${module.os_img_prd.state.project_id}"]
#   exclude_projects  = local.os_image_exclude_projects

# }

module "compute-vm-external-ip-access" {
  source  = "terraform-google-modules/org-policy/google"
  version = "~> 3.0"

  constraint       = "constraints/compute.vmExternalIpAccess"
  policy_type      = "list"
  policy_for       = "folder"
  folder_id        = "${module.folders.state.redacted_folder_id}"
  allow            = module.info.vm_allowed_external_ip
  exclude_projects = [module.transit_network.state.project_id]
}

module "compute-disable-serial-port-access" {
  source  = "terraform-google-modules/org-policy/google"
  version = "~> 3.0"

  constraint  = "constraints/compute.disableSerialPortAccess"
  policy_for  = "folder"
  policy_type = "boolean"
  folder_id   = "${module.folders.state.redacted_folder_id}"
}

module "compute-skip-default-network-creation" {
  source  = "terraform-google-modules/org-policy/google"
  version = "~> 3.0"

  constraint  = "constraints/compute.skipDefaultNetworkCreation"
  policy_for  = "folder"
  policy_type = "boolean"
  folder_id   = "${module.folders.state.redacted_folder_id}"
}

#AIM Policies
# module "iam-allowed-policy-member-domains" {
#   source  = "terraform-google-modules/org-policy/google"
#   version = "~> 3.0"

#   constraint        = "constraints/iam.allowedPolicyMemberDomains"
#   policy_type       = "list"
#   policy_for        = "folder"
#   folder_id         = "${module.folders.state.redacted_folder_id}"
#   enforce           = null
#   allow_list_length = length(module.info.allowed_domains)
#   allow             = module.info.allowed_domains
# }

module "iam-disable-service-account-key-creation" {
  source  = "terraform-google-modules/org-policy/google"
  version = "~> 3.0"

  constraint  = "iam.disableServiceAccountKeyCreation"
  policy_for  = "folder"
  policy_type = "boolean"
  folder_id   = "${module.folders.state.redacted_folder_id}"
  exclude_projects = [
    module.cloud_build_project.state.project_id,
    module.org_cloud_build_project.state.project_id,
    module.dev_security_org_sink_project.state.project_id,
    module.prod_security_org_sink_project.state.project_id,
    module.prod_security_dome9.state.project_id,
    module.dev_gsuite_export.state.project_id,
    module.prod_gsuite_export.state.project_id
  ]
}

#Cloud Resource Manager Policies
module "compute-restrict-xpn-project-lien-removal" {
  source  = "terraform-google-modules/org-policy/google"
  version = "~> 3.0"

  constraint  = "constraints/compute.restrictXpnProjectLienRemoval"
  policy_for  = "folder"
  policy_type = "boolean"
  folder_id   = "${module.folders.state.redacted_folder_id}"
}

#Google Cloud Platform Policies
module "gcp-resource-locations" {
  source  = "terraform-google-modules/org-policy/google"
  version = "~> 3.0"

  constraint        = "constraints/gcp.resourceLocations"
  policy_type       = "list"
  policy_for        = "folder"
  folder_id         = "${module.folders.state.redacted_folder_id}"
  allow             = ["in:us-locations"]
  enforce           = null
  allow_list_length = 1
  #  exclude_projects  = [module.dlp_scanner_project.state.project_id]
}

#Cloud Storage Policies
module "storage-uniform-bucket-level-access" {
  source  = "terraform-google-modules/org-policy/google"
  version = "~> 3.0"

  constraint  = "constraints/storage.uniformBucketLevelAccess"
  policy_for  = "folder"
  policy_type = "boolean"
  folder_id   = "${module.folders.state.redacted_folder_id}"
  exclude_projects = [
    module.dev_security_org_sink_project.state.project_id,
    module.prod_security_org_sink_project.state.project_id,
    module.dev_gsuite_export.state.project_id,
    module.prod_gsuite_export.state.project_id
  ]
}

#Cloud SQL Policies
module "sql-restrict-public-ip" {
  source  = "terraform-google-modules/org-policy/google"
  version = "~> 3.0"

  constraint  = "constraints/sql.restrictPublicIp"
  policy_for  = "folder"
  policy_type = "boolean"
  folder_id   = "${module.folders.state.redacted_folder_id}"
}
