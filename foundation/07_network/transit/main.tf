# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "folders" {
  source = "../../../modules/remote"

  component = "folders"
  env       = "core"
}

module "transit_network" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "transit_network"
}

module "shared_vpc" {
  source = "../../../modules/network"

  project_id           = module.transit_network.state.project_id
  env                  = "transit"
  cidr_block           = module.info.cidr_block.transit
  regions              = module.info.regions
  blk_ranges           = module.info.blk_ranges
  monitoring_ranges    = module.info.monitoring_ranges
  tcp_monitoring_ports = module.info.tcp_monitoring_ports
  udp_monitoring_ports = module.info.udp_monitoring_ports
}

module "router_a" {
  source = "../../../modules/router"

  project_id = module.transit_network.state.project_id
  network    = module.shared_vpc.network_name
  region     = "us_a"
}

module "router_b" {
  source = "../../../modules/router"

  project_id = module.transit_network.state.project_id
  network    = module.shared_vpc.network_name
  region     = "us_b"
}
