# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "network_name" {
  description = "The name of the shared VPC Created"
  value       = module.shared_vpc.network_name
}

output "network_self_link" {
  description = "The URI of the VPC being created"
  value       = module.shared_vpc.network_self_link
}

output "subnets_names" {
  description = "The names of the subnets being created"
  value       = module.shared_vpc.subnets_names
}

output "subnets_self_links" {
  description = "The self-links of subnets being created"
  value       = module.shared_vpc.subnets_self_links
}