# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "network_name" {
  description = "The name of the shared VPC Created"
  value       = module.network.shared_vpc.network_name
}

output "network_self_link" {
  description = "The URI of the VPC being created"
  value       = module.network.shared_vpc.network_self_link
}
