# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

locals {
  group = module.info.iam_groups["security_admin"][0]
}

module "project" {
  source = "../../../modules/blk_project_factory"

  resource_owner = local.group
  admin_group    = local.group

  connect_shared_vpc = true
  core               = false
  business_unit      = "test"
  environment        = "dev"
  cost_center        = module.info.cost_centers["cloud"]
  detail             = "prj-factory2"
  application_id     = "prj_factory2"
  data_class         = "prj_factory2"
  additional_apis    = ["servicenetworking.googleapis.com"]
}
