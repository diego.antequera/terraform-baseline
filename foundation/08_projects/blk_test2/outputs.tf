# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "project_id" {
  description = "id of the project that was created"
  value       = module.project.project_id
}
