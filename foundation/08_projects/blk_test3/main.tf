# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

locals {
  group = module.info.iam_groups["org_admin"][0]
}

module "auto_labelling" {
  source = "../../../modules/blk_project_factory/modules/blk_project"

  resource_owner = local.group
  admin_group    = local.group

  core           = false
  business_unit  = "core"
  environment    = "dev"
  cost_center    = module.info.cost_centers["cloud"]
  detail         = "auto-tstb"
  application_id = "core"
  data_class     = "core"
}

resource "google_project_service" "project_services" {
  project = module.auto_labelling.project_id
  service = "appengine.googleapis.com"
}

// Cloud Scheduler Requires App Engine Application to be deployed in Project. See https://cloud.google.com/scheduler/docs/
resource "google_app_engine_application" "app" {
  project     = module.auto_labelling.project_id
  location_id = module.info.regions.us_a
}
