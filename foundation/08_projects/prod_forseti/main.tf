# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

locals {
  group = module.info.iam_groups["security_admin"][0]
}

module "project" {
  source = "../../../modules/blk_project_factory"

  resource_owner = local.group
  admin_group    = local.group

  connect_shared_vpc = true
  core               = true
  business_unit      = "security"
  environment        = "prod"
  cost_center        = module.info.cost_centers["cloud"]
  detail             = "forseti"
  application_id     = "security"
  data_class         = "security"
  additional_apis    = ["servicenetworking.googleapis.com"]
}
