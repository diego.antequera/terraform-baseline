# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "auto_label" {
  source = "../../../modules/env_auto_label"
  env    = "dev"
}

