module "info" {
  source = "../../../modules/info"
}

module "project" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "transit_network"
}

module "network" {
  source = "../../../modules/remote"

  component = "network"
  env       = "transit"
}

locals {
  import_files = [
    "common.py",
    "default.py",
    "vm_instance.py",
    "vm.jinja",
    "c2d_deployment_configuration.json",
    "software_status.py",
    "software_status.sh.tmpl",
    "software_status.py.schema",
    "software_status_script.py",
    "software_status_script.py.schema"
  ]

  instances = {
    01 = {
      zone       = module.info.zones.us_a
      subnetwork = module.network.state.subnets_names[0]
    }
    02 = {
      zone       = module.info.zones.us_a
      subnetwork = module.network.state.subnets_names[0]
    }
    03 = {
      zone       = module.info.zones.us_b
      subnetwork = module.network.state.subnets_names[1]
    }
    04 = {
      zone       = module.info.zones.us_b
      subnetwork = module.network.state.subnets_names[1]
    }
  }
}

resource "google_deployment_manager_deployment" "csr" {
  for_each = local.instances
  name     = "${module.info.prefix}-transit-csr-${each.key}"
  project  = module.project.state.project_id

  target {
    config {
      content = <<EOF
imports:
- path: vm.jinja

resources:
- name: csr-${each.key}
  type: vm.jinja
  properties:
    project: ${module.project.state.project_id}
    zone: "${each.value.zone}"
    network: "${module.network.state.network_self_link}"
    instanceName: "${module.info.prefix}-transit-csr-${each.key}-vm"
    userName: ${module.info.csr_username}
    instanceKeys: ${module.info.csr_ssh_key}
    machineType: "n1-standard-4"
    subnetwork: "${each.value.subnetwork}"
    bootDiskType: "pd-ssd"
    bootDiskSizeGb: 10.0
    externalIP: "Ephemeral"
    ipForward: "On"
    numAdditionalNICs: 0.0
    enableTcp22: "true"
    tcp22SourceRanges: ""
    enableTcp80: "true"
    tcp80SourceRanges: ""
EOF
    }

    dynamic "imports" {
      for_each = local.import_files
      content {
        name    = imports.value
        content = file("template/${imports.value}")
      }
    }
  }
}

resource "google_compute_route" "default" {
  count = length(google_deployment_manager_deployment.csr)

  name                   = "${values(google_deployment_manager_deployment.csr)[count.index]["name"]}-onprem-route"
  dest_range             = "45.0.0.0/8"
  project                = module.project.state.project_id
  network                = module.network.state.network_name
  priority               = 1000
  next_hop_instance      = "${values(google_deployment_manager_deployment.csr)[count.index]["id"]}-vm"
  next_hop_instance_zone = values(local.instances)[count.index]["zone"]
}
