# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "sb_project" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "sb_network"
}

module "sb_network" {
  source = "../../../modules/remote"

  component = "network"
  env       = "sandbox"
}

locals {
  name        = "${module.info.prefix}-gcp-sb-private-zone"
  doamin_name = "sb.${module.info.gcp_base_domain}."
  networks    = [module.sb_network.state.network_self_link]
}

module "activate_dns_api" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  version = "~> 4.0"

  project_id    = module.sb_project.state.project_id
  activate_apis = ["dns.googleapis.com"]
}

module "dns-private-zone" {
  source  = "terraform-google-modules/cloud-dns/google"
  version = "~> 3.0"

  project_id                         = module.sb_project.state.project_id
  type                               = "private"
  name                               = local.name
  description                        = "The private zone for ${local.doamin_name}"
  domain                             = local.doamin_name
  private_visibility_config_networks = local.networks
  recordsets = [
    {
      name = "ns"
      type = "A"
      ttl  = 300
      records = [
        "127.0.0.1",
      ]
    },
    {
      name = ""
      type = "NS"
      ttl  = 300
      records = [
        "ns.${local.doamin_name}",
      ]
    },
    {
      name = "localhost"
      type = "A"
      ttl  = 300
      records = [
        "127.0.0.1",
      ]
    },
    {
      name = ""
      type = "MX"
      ttl  = 300
      records = [
        "1 localhost.",
      ]
    },
    {
      name = ""
      type = "TXT"
      ttl  = 300
      records = [
        "\"v=spf1 -all\"",
      ]
    },
  ]
}
