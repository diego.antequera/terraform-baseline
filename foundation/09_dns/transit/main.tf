# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "transit_project" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "transit_network"
}

module "transit_network" {
  source = "../../../modules/remote"

  component = "network"
  env       = "transit"
}

module "prod_network" {
  source = "../../../modules/remote"

  component = "network"
  env       = "prod"
}

module "tst_network" {
  source = "../../../modules/remote"

  component = "network"
  env       = "tst"
}

module "dev_network" {
  source = "../../../modules/remote"

  component = "network"
  env       = "dev"
}

locals {
  name             = "${module.info.prefix}-gcp-transit-private-zone"
  domain_name      = "transit.${module.info.gcp_base_domain}."
  prod_domain_name = "prod.${module.info.gcp_base_domain}."
  tst_domain_name  = "tst.${module.info.gcp_base_domain}."
  dev_domain_name  = "dev.${module.info.gcp_base_domain}."
  networks         = [module.transit_network.state.network_self_link]
}

module "dns-peering-zone-prod" {
  source  = "terraform-google-modules/cloud-dns/google"
  version = "~> 3.0"

  project_id                         = module.transit_project.state.project_id
  type                               = "peering"
  name                               = "${local.name}-peering-prod"
  domain                             = local.prod_domain_name
  private_visibility_config_networks = local.networks
  target_network                     = module.prod_network.state.network_self_link
}

module "dns-peering-zone-tst" {
  source  = "terraform-google-modules/cloud-dns/google"
  version = "~> 3.0"

  project_id                         = module.transit_project.state.project_id
  type                               = "peering"
  name                               = "${local.name}-peering-tst"
  domain                             = local.tst_domain_name
  private_visibility_config_networks = local.networks
  target_network                     = module.tst_network.state.network_self_link
}

module "dns-peering-zone-dev" {
  source  = "terraform-google-modules/cloud-dns/google"
  version = "~> 3.0"

  project_id                         = module.transit_project.state.project_id
  type                               = "peering"
  name                               = "${local.name}-peering-dev"
  domain                             = local.dev_domain_name
  private_visibility_config_networks = local.networks
  target_network                     = module.dev_network.state.network_self_link
}

module "dns-private-zone" {
  source  = "terraform-google-modules/cloud-dns/google"
  version = "~> 3.0"

  project_id                         = module.transit_project.state.project_id
  type                               = "private"
  name                               = local.name
  description                        = "The private zone for ${local.domain_name}"
  domain                             = local.domain_name
  private_visibility_config_networks = local.networks
  recordsets = [
    {
      name = "ns"
      type = "A"
      ttl  = 300
      records = [
        "127.0.0.1",
      ]
    },
    {
      name = ""
      type = "NS"
      ttl  = 300
      records = [
        "ns.${local.domain_name}",
      ]
    },
    {
      name = "localhost"
      type = "A"
      ttl  = 300
      records = [
        "127.0.0.1",
      ]
    },
    {
      name = ""
      type = "MX"
      ttl  = 300
      records = [
        "1 localhost.",
      ]
    },
    {
      name = ""
      type = "TXT"
      ttl  = 300
      records = [
        "\"v=spf1 -all\"",
      ]
    },
  ]
}
