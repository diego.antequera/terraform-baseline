# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.
module "forseti" {
  source = "../../../modules/env_forseti"

  env               = "dev"
  cloud_nat_enabled = true
}
