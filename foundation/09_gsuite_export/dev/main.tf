# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "gsuite_export_project" {
  source = "../../../modules/remote"

  component = "projects"
  env       = "dev_gsuite_export"
}

module "service_accounts" {
  source = "../../../modules/remote"

  component = "service_accounts"
  env       = "dev"
}

module "gsuite-exporter" {
  source                          = "../../../modules/external/gsuite_export_cf"
  region                          = module.info.regions.us_a
  project_id                      = module.gsuite_export_project.state.project_id
  name                            = "${module.info.prefix}-dev-core-gsuite-export_us-a"
  cs_schedule                     = "*/10 * * * *"
  gsuite_exporter_service_account = module.service_accounts.state.gsuite_export_service_accounts_emails[0]
  gsuite_admin_user               = module.info.gsuite_admin
  description                     = "GSuite Logs Exporter"
}
