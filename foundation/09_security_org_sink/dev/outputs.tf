output "pubsub_writer_identity" {
  description = "The service account that logging uses to write log entries to the destination."
  value       = module.org_sink.pubsub_writer_identity
}
