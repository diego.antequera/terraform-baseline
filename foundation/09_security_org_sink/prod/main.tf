# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../../modules/info"
}

module "org_sink" {
  source = "../../../modules/env_org_sink"
  env    = "prod"
}

module "org_logs_export" {
  source                 = "../../../modules/external/log_export"
  destination_uri        = module.org_sink.destination_uri
  log_sink_name          = "${module.info.prefix}-prod-core-security-org-log-sink"
  parent_resource_id     = module.info.org_id
  parent_resource_type   = "organization"
  unique_writer_identity = false
  include_children       = false
}
