output "storage_writer_identity" {
  description = "The service account that logging uses to write log entries to the storage destination."
  value       = module.org_sink.storage_writer_identity
}

output "pubsub_writer_identity" {
  description = "The service account that logging uses to write log entries to the destination."
  value       = module.org_sink.pubsub_writer_identity
}

output "pubsub_subscriber" {
  description = "The service account has subscribe access to the export pubsub"
  value       = module.org_sink.pubsub_subscriber
}
