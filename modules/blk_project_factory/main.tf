# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../info"
}

module "prod_network" {
  source = "../remote"

  component = "network"
  env       = "prod"
}

module "tst_network" {
  source = "../remote"

  component = "network"
  env       = "tst"
}

module "dev_network" {
  source = "../remote"

  component = "network"
  env       = "dev"
}

module "sb_network" {
  source = "../remote"

  component = "network"
  env       = "sandbox"
}

module "prod_net_project" {
  source = "../remote"

  component = "projects"
  env       = "prod_network"
}

module "tst_net_project" {
  source = "../remote"

  component = "projects"
  env       = "tst_network"
}

module "dev_net_project" {
  source = "../remote"

  component = "projects"
  env       = "dev_network"
}

module "sb_net_project" {
  source = "../remote"

  component = "projects"
  env       = "sb_network"
}

locals {
  network_project = {
    prod = module.prod_net_project.state.project_id
    tst  = module.tst_net_project.state.project_id
    dev  = module.dev_net_project.state.project_id
    sb   = module.sb_net_project.state.project_id
  }

  network = {
    prod = module.prod_network.state.network_name
    tst  = module.tst_network.state.network_name
    dev  = module.dev_network.state.network_name
    sb   = module.sb_network.state.network_name
  }

  service_acc_names = {
    dmz      = "${var.business_unit}-${var.detail}-dmz-sa"
    jump     = "${var.business_unit}-${var.detail}-jump-sa"
    internal = "${var.business_unit}-${var.detail}-internal-sa"
  }
}

#Create the project and attach it to a shared vpc
module "blk_project_factory" {
  source = "./modules/blk_project"

  network_project = var.connect_shared_vpc ? local.network_project[var.environment] : ""
  resource_owner  = var.resource_owner
  application_id  = var.application_id
  business_unit   = var.business_unit
  core            = var.core
  environment     = var.environment
  data_class      = var.data_class
  cost_center     = var.cost_center
  detail          = var.detail
  client_name     = var.client_name
  app_version     = var.app_version
  project         = var.project
  app_component   = var.app_component
  admin_group     = var.admin_group
  budget_amount   = var.budget_alert_amount
  additional_apis = var.additional_apis
}

#service accounts for the firewall
module "firewall_service_accounts" {
  source  = "terraform-google-modules/service-accounts/google"
  version = "~> 2.0"

  project_id    = module.blk_project_factory.project_id
  names         = [local.service_acc_names.dmz, local.service_acc_names.jump, local.service_acc_names.internal]
  project_roles = []
}

module "project_firewall_rules" {
  source = "./modules/firewall"

  project_id                  = local.network_project[var.environment]
  service_project_id          = module.blk_project_factory.project_id
  network_name                = local.network[var.environment]
  expose_applications         = var.connect_shared_vpc
  dmz_sa                      = module.firewall_service_accounts.service_accounts[0].email
  jump_sa                     = module.firewall_service_accounts.service_accounts[1].email
  internal_sa                 = module.firewall_service_accounts.service_accounts[2].email
  blk_range                   = module.info.blk_ranges
  application_ports           = module.info.tcp_application_ports
  allow_internal_egress_ports = module.info.allow_internal_egress_ports
}

module "project_kms" {
  source        = "./modules/kms"
  environment   = var.environment
  business_unit = var.business_unit
  location      = module.info.regions.us_a
  detail        = var.detail
  project_id    = module.blk_project_factory.project_id
  admin_group   = var.admin_group

}

module "project_dome9" {
  source     = "./modules/dome9"
  project_id = module.blk_project_factory.project_id
}
