# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.
module "info" {
  source = "../../../info"
}

module "folders" {
  source = "../../../remote"

  component = "folders"
  env       = "root"
}

module "core_folders" {
  source = "../../../remote"

  component = "folders"
  env       = "core"
}

locals {
  name = "${module.info.prefix}-${var.environment}-${var.business_unit}-${var.detail}"
  labels = {
    blk-resource-owner = lower(split("@", var.resource_owner)[0])
    blk-application-id = lower(var.application_id)
    blk-business-unit  = lower(var.business_unit)
    blk-environment    = lower(var.environment)
    blk-data-class     = lower(var.data_class)
    blk-costcenter     = lower(var.cost_center)
    blk-client-name    = lower(var.client_name)
    blk-app-version    = lower(var.app_version)
    blk-project        = lower(var.project)
    blk-app-component  = lower(var.app_component)
  }

  folders = {
    prod = module.folders.state.root_prod_folders_id
    tst  = module.folders.state.root_tst_folders_id
    dev  = module.folders.state.root_dev_folders_id
    sb   = module.folders.state.root_sb_folders_id
  }

  core_folders = merge(module.core_folders.state.folder_ids, { transit = module.core_folders.state.folder_ids["prod"] })
}

#Create the project and attach it to a shared vpc
module "blk_project" {
  source = "../../../external/project_factory"

  #project config
  name              = local.name
  org_id            = module.info.org_id
  folder_id         = var.core ? local.core_folders[var.environment == "sb" ? "sb" : var.environment] : local.folders[var.environment == "sb" ? "sb" : var.environment]
  billing_account   = module.info.billing_account
  random_project_id = false
  labels            = local.labels

  #don't try to delete the default network, was removed using org-policies
  auto_create_network = true

  #security elements
  default_service_account = "disable"
  lien                    = "true"

  #acivate apis
  activate_apis = concat(module.info.project_activate_apis, var.additional_apis)

  #Shared VPC Connection
  shared_vpc = var.network_project

  #budget amount
  budget_amount = var.budget_amount == "" ? module.info.budget_alert_amount : var.budget_amount
}

#give the permissions for the group
module "project-iam-bindings" {
  source  = "terraform-google-modules/iam/google//modules/projects_iam"
  version = "~>5.1"

  projects = [module.blk_project.project_id]
  mode     = "additive"
  bindings = {
    #databases
    "roles/bigquery.admin" = ["group:${var.admin_group}"]
    "roles/bigtable.admin" = ["group:${var.admin_group}"]
    "roles/cloudsql.admin" = ["group:${var.admin_group}"]
    "roles/redis.admin"    = ["group:${var.admin_group}"]
    "roles/spanner.admin"  = ["group:${var.admin_group}"]
    "roles/storage.admin"  = ["group:${var.admin_group}"]

    #compute
    "roles/cloudfunctions.admin"      = ["group:${var.admin_group}"]
    "roles/compute.networkUser"       = ["group:${var.admin_group}"]
    "roles/compute.imageUser"         = ["group:${var.admin_group}"]
    "roles/compute.instanceAdmin.v1"  = ["group:${var.admin_group}"]
    "roles/compute.loadBalancerAdmin" = ["group:${var.admin_group}"]
    "roles/container.developer"       = ["group:${var.admin_group}"]
    "roles/container.clusterViewer"   = ["group:${var.admin_group}"]
    "roles/container.admin"           = ["group:${var.admin_group}"]
    "roles/containeranalysis.admin"   = ["group:${var.admin_group}"]

    #data processing
    "roles/composer.admin"    = ["group:${var.admin_group}"]
    "roles/datacatalog.admin" = ["group:${var.admin_group}"]
    "roles/dlp.admin"         = ["group:${var.admin_group}"]
    "roles/dataflow.admin"    = ["group:${var.admin_group}"]
    "roles/dataproc.admin"    = ["group:${var.admin_group}"]
    "roles/pubsub.admin"      = ["group:${var.admin_group}"]

    #development
    "roles/binaryauthorization.attestorsAdmin" = ["group:${var.admin_group}"]
    "roles/binaryauthorization.policyAdmin"    = ["group:${var.admin_group}"]
    "roles/cloudbuild.builds.builder"          = ["group:${var.admin_group}"]

    #security
    "roles/cloudkms.admin"          = ["group:${var.admin_group}"]
    "roles/iam.serviceAccountAdmin" = ["group:${var.admin_group}"]
    "roles/iam.serviceAccountUser"  = ["group:${var.admin_group}"]

    #services
    "roles/automl.admin"         = ["group:${var.admin_group}"]
    "roles/cloudtranslate.admin" = ["group:${var.admin_group}"]
    "roles/cloudscheduler.admin" = ["group:${var.admin_group}"]

    #monitoring and logging
    "roles/clouddebugger.user" = ["group:${var.admin_group}"]
    "roles/cloudprofiler.user" = ["group:${var.admin_group}"]
    "roles/cloudtrace.admin"   = ["group:${var.admin_group}"]
    "roles/logging.admin"      = ["group:${var.admin_group}"]
    "roles/monitoring.admin"   = ["group:${var.admin_group}"]
  }
}
