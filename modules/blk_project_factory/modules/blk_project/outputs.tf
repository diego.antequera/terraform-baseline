# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "project_id" {
  description = "The project id created for redacted"
  value       = module.blk_project.project_id
}

output "project_number" {
  description = "The project number created for redacted"
  value       = module.blk_project.project_number
}


