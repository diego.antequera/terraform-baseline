# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.


variable "network_project" {
  description = "Network Project that will need to be connected to."
  type        = string
  default     = ""
}

variable "resource_owner" {
  description = "Used to identify who is responsible for the project. This should be a group email list, avoiding individual's own email addresses"
  type        = string
}

variable "application_id" {
  description = "Used to identify that the project is linked with a specific redacted application. This should reference a Beast application"
  type        = string
}

variable "business_unit" {
  description = "Used to identify which BU this project is built for"
  type        = string
}

variable "core" {
  description = "Used to identify core projects"
  type        = bool
  default     = false
}

variable "environment" {
  description = "Used to identify which environment of the GCP project. Values can be (sb, dev, tst, prod)"
  type        = string
}

variable "data_class" {
  description = "Used to label resources that process or store data, consistent with redacted data classification policy. In case of mixed data classes, this tag should reflect the highest classification"
  type        = string
}

variable "cost_center" {
  description = "Used to identify the cost center that could be used to cover the cost of this resource"
  type        = string
}

variable "detail" {
  description = "Used to diferentiate name the project "
  type        = string
}

variable "client_name" {
  description = "(Optional) Used to identify which client this resource supports"
  type        = string
  default     = ""
}

variable "app_version" {
  description = "(Optional) Used to identify which version of the software this resource supports"
  type        = string
  default     = ""
}

variable "project" {
  description = "(Optional) Used to identify a specific redacted project within an application ID"
  type        = string
  default     = ""
}

variable "app_component" {
  description = "(Optional) Used to identify a grouping of resources for a specific purpose within an application. Useful to distinguish between frontend and backend"
  type        = string
  default     = ""
}

variable "admin_group" {
  description = "Group that will be given the administration priviledge to the project"
  type        = string
}

variable "budget_amount" {
  description = "Budget abount to be used"
  type        = string
  default     = ""
}

variable "additional_apis" {
  description = "Additional APIs that needs to be activated"
  type        = list(string)
  default     = []
}
