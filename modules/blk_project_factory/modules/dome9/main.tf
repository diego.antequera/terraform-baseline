# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "dome9_project" {
  source = "../../../remote"

  component = "projects"
  env       = "prod_dome9"
}

data "google_secret_manager_secret_version" "dome9_access_id" {
  project  = module.dome9_project.state.project_id
  provider = google-beta
  secret   = "dome9_access_id"
}

data "google_secret_manager_secret_version" "dome9_secret_key" {
  project  = module.dome9_project.state.project_id
  provider = google-beta
  secret   = "dome9_secret_key"
}

provider "dome9" {
  dome9_access_id  = data.google_secret_manager_secret_version.dome9_access_id.secret_data
  dome9_secret_key = data.google_secret_manager_secret_version.dome9_secret_key.secret_data
}

module "info" {
  source = "../../../info"
}

module "service_accounts" {
  source = "../../../remote"

  component = "service_accounts"
  env       = "org"
}

resource "dome9_cloudaccount_gcp" "gcp_ca" {
  name                   = var.project_id
  project_id             = var.project_id
  private_key_id         = module.service_accounts.state.dome9_key.private_key_id
  private_key            = module.service_accounts.state.dome9_key.private_key
  client_email           = module.service_accounts.state.dome9_key.client_email
  client_id              = module.service_accounts.state.dome9_key.client_id
  client_x509_cert_url   = module.service_accounts.state.dome9_key.client_x509_cert_url
  organizational_unit_id = module.info.dome9_organizational_unit
}
