# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.
locals {
  range_rules = {
    a-https-i-dmz-f-blk = {
      prefix               = var.service_project_id
      description          = "Allow ingress HTTP/S ports from BLK Ranges on DMZ Service Account VMs"
      direction            = "INGRESS"
      action               = "allow"
      ranges               = var.blk_range
      use_service_accounts = true
      sources              = null
      targets              = [var.dmz_sa]
      rules = [
        {
          protocol = "TCP"
          ports    = ["80", "443"]
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
    a-t-i-dmz-int-f-gcp = {
      prefix               = var.service_project_id
      description          = "Allow ingress from Google for health checks and trafic."
      direction            = "INGRESS"
      action               = "allow"
      ranges               = ["35.191.0.0/16", "130.211.0.0/22"]
      use_service_accounts = true
      sources              = null
      targets              = [var.dmz_sa, var.internal_sa]
      rules = [
        {
          protocol = "TCP"
          ports    = var.application_ports
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
    a-sshrdp-i-jmp-f-blk = {
      prefix               = var.service_project_id
      description          = "Allow ingress SSH & RDP from BLK Ranges"
      direction            = "INGRESS"
      action               = "allow"
      ranges               = var.blk_range
      use_service_accounts = true
      sources              = null
      targets              = [var.jump_sa]
      rules = [
        {
          protocol = "TCP"
          ports    = ["22", "3389"]
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
    a-sshrdp-e-jmp-t-blk = {
      prefix               = var.service_project_id
      description          = "Allow SSH & RDP egress from Jump Service Account VMs to redacted Ranges"
      direction            = "EGRESS"
      action               = "allow"
      ranges               = var.blk_range
      use_service_accounts = true
      sources              = null
      targets              = [var.jump_sa]
      rules = [
        {
          protocol = "TCP"
          ports    = ["22", "3389"]
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
    a-t-e-int-t-dmz = {
      prefix               = var.service_project_id
      description          = "Allow egress traffic from DMZ Service Account VMs to redacted ranges"
      direction            = "EGRESS"
      action               = "allow"
      ranges               = var.blk_range
      use_service_accounts = true
      sources              = null
      targets              = [var.dmz_sa]
      rules = [
        {
          protocol = "TCP"
          ports    = var.application_ports
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
    a-e-int-to-int = {
      prefix               = var.service_project_id
      description          = "Allow egress traffic from Internal Service Account VMs to redacted ranges"
      direction            = "EGRESS"
      action               = "allow"
      ranges               = var.blk_range
      use_service_accounts = true
      sources              = null
      targets              = [var.internal_sa]
      rules = [
        {
          protocol = "TCP"
          ports    = var.allow_internal_egress_ports
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
  }

  sa_rules = {
    a-t-i-int-f-dmz = {
      prefix               = var.service_project_id
      description          = "Allow ingress from DMZ Service Account VMs to  Internal Service Account VMs."
      direction            = "INGRESS"
      action               = "allow"
      ranges               = null
      use_service_accounts = true
      sources              = [var.dmz_sa]
      targets              = [var.internal_sa]
      rules = [
        {
          protocol = "TCP"
          ports    = var.application_ports
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
    a-sshrdp-i-int-dmz-f-jmp = {
      prefix               = var.service_project_id
      description          = "Allow SSH & RDP ingress from Jump Service Account VMs to Internal & DMZ Service Accounts VMs"
      direction            = "INGRESS"
      action               = "allow"
      ranges               = null
      use_service_accounts = true
      sources              = [var.jump_sa]
      targets              = [var.internal_sa, var.dmz_sa]
      rules = [
        {
          protocol = "TCP"
          ports    = ["22", "3389"]
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
    a-i-int-f-int = {
      prefix               = var.service_project_id
      description          = "Allow ingress traffic from Internal Service Account VMs to Internal Service Account VMs"
      direction            = "INGRESS"
      action               = "allow"
      ranges               = null
      use_service_accounts = true
      sources              = [var.internal_sa]
      targets              = [var.internal_sa]
      rules = [
        {
          protocol = "ALL"
          ports    = []
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
  }
}

module "range_firewall_rules" {
  source = "../../../firewall"

  project_id   = var.project_id
  network_name = var.network_name
  custom_rules = var.expose_applications ? local.range_rules : {}
}

module "service_acc_firewall_rules" {
  source = "../../../firewall"

  project_id   = var.project_id
  network_name = var.network_name
  custom_rules = var.expose_applications ? local.sa_rules : {}
}
