# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

variable "project_id" {
  description = "The project id where the firewall rules must be added"
  type        = string
}

variable "network_name" {
  description = "The name of the network where the firewall rules must be added"
  type        = string
}

variable "expose_applications" {
  description = "Indicate if this project will have any exposed project that need to be accessed, then the defined network application will be created."
  type        = bool
}

variable "dmz_sa" {
  description = "The DMZ service account"
  type        = string
}

variable "jump_sa" {
  description = "The Jump service account"
  type        = string
}

variable "internal_sa" {
  description = "The Internal service account"
  type        = string
}

variable "blk_range" {
  description = "The redacted routable origin IP range, GCP and on-prem"
  type        = list(string)
}

variable "application_ports" {
  description = "List of application ports exposed"
  type        = list(string)
}

variable "service_project_id" {
  description = "The project id for the service project"
  type        = string
}

variable "allow_internal_egress_ports" {
  description = "The ports that will be allowed to egress from Internal SA to BLK Ip Range"
  type        = list(string)
}

