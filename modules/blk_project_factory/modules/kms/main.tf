# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.
module "info" {
  source = "../../../info"
}

locals {
  keyring_name     = "${module.info.prefix}-gcp-${var.environment}-${var.business_unit}-${var.location}-${var.detail}-kring"
  default_key_name = "${local.keyring_name}-default-key"
}

module "kms" {
  source  = "terraform-google-modules/kms/google"
  version = "~> 1.1"

  project_id          = var.project_id
  location            = var.location
  keyring             = local.keyring_name
  keys                = [local.default_key_name]
  key_rotation_period = module.info.key_rotation_period
  prevent_destroy     = false

  set_owners_for     = [local.default_key_name]
  set_encrypters_for = [local.default_key_name]
  set_decrypters_for = [local.default_key_name]

  owners = [
    "group:${var.admin_group}",
  ]
  encrypters = [
    "group:${var.admin_group}",
  ]
  decrypters = [
    "group:${var.admin_group}",
  ]
}
