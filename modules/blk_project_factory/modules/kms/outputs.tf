# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "keyring_name" {
  description = "Name of the keyring generated"
  value       = module.kms.keyring_name
}

output "keys" {
  description = "Keys generated"
  value       = module.kms.keys
}

