# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

variable "environment" {
  description = "Used to identify which environment of the keys. Values can be (sb, dev, tst, prod)"
  type        = string
}

variable "business_unit" {
  description = "Used to identify which BU this project is built for"
  type        = string
}

variable "location" {
  description = "Location which the KMS will be created"
  type        = string
}

variable "detail" {
  description = "Used to diferentiate name the keyring"
  type        = string
}

variable "project_id" {
  description = "Project id to which these KMS belong"
  type        = string
}

variable "admin_group" {
  description = "Group that will be given the administration priviledge to the keys"
  type        = string
}
