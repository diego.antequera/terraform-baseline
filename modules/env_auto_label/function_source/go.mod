module resource-auto-labeller

go 1.11

require (
	cloud.google.com/go/pubsub v1.2.0
	google.golang.org/api v0.16.0
)
