package labelResource

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"regexp"

	"cloud.google.com/go/pubsub"
	"google.golang.org/api/compute/v1"
)

type creationEvent struct {
	JsonPayload struct {
		Actor struct {
			UserID			string `json:"user"`
		} `json:"actor"`
	} `json:"jsonPayload"`
	Resource struct {
		Labels struct {
			InstanceID string `json:"instance_id"`
			ProjectID  string `json:"project_id"`
			Zone       string `json:"zone"`
		} `json:"labels"`
	} `json:"resource"`
}

var computeService *compute.Service

func init() {
	var err error

	computeService, err = compute.NewService(context.Background())
	if err != nil {
		log.Fatalf("Could not create compute service: %v\n", err)
	}
}

func ReceiveMessage(ctx context.Context, msg *pubsub.Message) error {
	var event creationEvent
	json.Unmarshal([]byte(msg.Data), &event)
	labels := event.Resource.Labels

	var re = regexp.MustCompile(`@.*`)
	owner := re.ReplaceAllString(event.JsonPayload.Actor.UserID, ``)

	fmt.Printf("VM Created; adding label. %v\n", labels)
	fmt.Printf("Splunk Logs. %v\n", event)

	projectID := labels.ProjectID
	zoneID := labels.Zone
	instanceID := labels.InstanceID

	inst, err := computeService.Instances.Get(projectID, zoneID, instanceID).Do()
	if err != nil {
		fmt.Printf("Error while trying to recover vm instance. Error: %v\n", err)
		return err
	}

	existingLabels := inst.Labels
	if existingLabels == nil {
			existingLabels = make(map[string]string)
	}

	if existingLabels["blk-resource-owner"] != "" {
		fmt.Printf("VM instance already has the label 'blk-resource-owner'. ", existingLabels)
		return nil
	}

	existingLabels["blk-resource-owner"] = owner
	fmt.Printf("Applying labels:", existingLabels)
	
	rb := &compute.InstancesSetLabelsRequest {
    LabelFingerprint: inst.LabelFingerprint,
    Labels:           existingLabels,
	}

	resp, err := computeService.Instances.SetLabels(projectID, zoneID, instanceID, rb).Do()
	if err != nil {
		fmt.Printf("Failed to request to add labels. Error: %v\n", err)
		return err
	}
	fmt.Printf("%#v\n", resp)
	return nil
}

