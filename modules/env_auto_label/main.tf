# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../modules/info"
}

module "folders" {
  source    = "../../modules/remote"
  component = "folders"
  env       = "root"
}

module "cloud_build_project" {
  source    = "../../modules/remote"
  component = "projects"
  env       = "prod_org_cloud_build"
}

module "auto_label_project" {
  source    = "../../modules/remote"
  component = "projects"
  env       = "${var.env}_auto_labelling"
}

module "event_folder_log_entry" {
  source = "../../modules/external/event_function/modules/event-folder-log-entry"

  filter           = "logName:activity resource.type=gce_instance jsonPayload.event_subtype=compute.instances.insert jsonPayload.event_type=GCE_OPERATION_DONE"
  name             = "${module.info.prefix}-${var.env}-core-auto-label-log"
  folder_id        = module.folders.state["root_${var.env}_folders_id"]
  project_id       = module.auto_label_project.state.project_id
  include_children = true
}

module "auto_label_service_account" {
  source     = "terraform-google-modules/service-accounts/google"
  version    = "~> 2.0"
  project_id = module.auto_label_project.state.project_id
  prefix     = "${module.info.prefix}-${var.env}"
  names      = ["autolabel-sa"]
}

resource "google_folder_iam_binding" "autolabel_function_folder_bindings" {
  folder = module.folders.state["root_${var.env}_folders_id"]
  role   = "roles/compute.instanceAdmin"

  members = module.auto_label_service_account.iam_emails_list
}

resource "google_service_account_iam_binding" "sa_binding" {
  service_account_id = "projects/${module.auto_label_project.state.project_id}/serviceAccounts/${module.auto_label_service_account.email}"
  role               = "roles/iam.serviceAccountUser"

  members = [
    "serviceAccount:${module.cloud_build_project.state.project_number}@cloudbuild.gserviceaccount.com"
  ]
}

module "auto_label_function" {
  # source = "terraform-google-modules/event-function/google"
  # version = "~> 1.2.0"
  source = "../../modules/external/event_function"

  description = "Auto label VMs with the resource owner Google Identity"
  entry_point = "ReceiveMessage"
  runtime     = "go111"
  timeout_s   = "240"

  event_trigger      = module.event_folder_log_entry.function_event_trigger
  name               = "${module.info.prefix}-${var.env}-core-auto-label_us-a"
  project_id         = module.auto_label_project.state.project_id
  region             = module.info.regions.us_a
  source_directory   = "${path.module}/function_source/"
  bucket_policy_only = true
  bucket_name        = "${module.info.prefix}-core-${var.env}-auto-label-func-bucket"

  service_account_email = module.auto_label_service_account.email
}
