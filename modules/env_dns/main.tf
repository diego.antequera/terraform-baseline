# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../modules/info"
}

module "project" {
  source = "../../modules/remote"

  component = "projects"
  env       = "${var.env}_network"
}

module "network" {
  source = "../../modules/remote"

  component = "network"
  env       = "${var.env}"
}

module "transit_network" {
  source = "../../modules/remote"

  component = "network"
  env       = "transit"
}

locals {
  name                = "${module.info.prefix}-gcp-${var.env}-zone"
  domain_name         = "${var.env}.${module.info.gcp_base_domain}."
  transit_domain_name = "transit.${module.info.gcp_base_domain}."
  on_prem_domain_name = "${module.info.gcp_base_domain}."
  networks            = [module.network.state.network_self_link]
}

module "dns-peering-zone-on-prem" {
  source  = "terraform-google-modules/cloud-dns/google"
  version = "~> 3.0"

  project_id                         = module.project.state.project_id
  type                               = "peering"
  name                               = "${local.name}-peering-on-prem"
  domain                             = local.on_prem_domain_name
  private_visibility_config_networks = local.networks
  target_network                     = module.transit_network.state.network_self_link
}

module "dns-peering-zone-transit" {
  source  = "terraform-google-modules/cloud-dns/google"
  version = "~> 3.0"

  project_id                         = module.project.state.project_id
  type                               = "peering"
  name                               = "${local.name}-peering-transit"
  domain                             = local.transit_domain_name
  private_visibility_config_networks = local.networks
  target_network                     = module.transit_network.state.network_self_link
}

module "dns-private-zone" {
  source  = "terraform-google-modules/cloud-dns/google"
  version = "~> 3.0"

  project_id                         = module.project.state.project_id
  type                               = "private"
  name                               = local.name
  description                        = "The private zone for ${local.domain_name}"
  domain                             = local.domain_name
  private_visibility_config_networks = local.networks
  recordsets = [
    {
      name = "ns"
      type = "A"
      ttl  = 300
      records = [
        "127.0.0.1",
      ]
    },
    {
      name = ""
      type = "NS"
      ttl  = 300
      records = [
        "ns.${local.domain_name}",
      ]
    },
    {
      name = "localhost"
      type = "A"
      ttl  = 300
      records = [
        "127.0.0.1",
      ]
    },
    {
      name = ""
      type = "MX"
      ttl  = 300
      records = [
        "1 localhost.",
      ]
    },
    {
      name = ""
      type = "TXT"
      ttl  = 300
      records = [
        "\"v=spf1 -all\"",
      ]
    },
  ]
}
