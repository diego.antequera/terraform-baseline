# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../modules/info"
}

module "forseti_project" {
  source = "../../modules/remote"

  component = "projects"
  env       = "${var.env}_forseti"
}

module "network_project" {
  source = "../../modules/remote"

  component = "projects"
  env       = "${var.env}_network"
}

module "network" {
  source = "../../modules/remote"

  component = "network"
  env       = "${var.env}"
}

module "servicenetworking" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  version = "~> 4.0"

  project_id    = module.network_project.state.project_id
  activate_apis = ["servicenetworking.googleapis.com"]
}

locals {
  forseti_server_tags = ["forseti-server"]
  forseti_client_tags = ["forseti-client"]
}
module "forseti" {
  source = "../../modules/external/forseti/5.1.1"

  //Forseti config
  gsuite_admin_email = module.info.forseti_gsuite_admin_email
  domain             = module.info.forseti_domain
  org_id             = module.info.org_id
  project_id         = module.forseti_project.state.project_id
  client_private     = "true"
  server_private     = "true"
  cloudsql_private   = "true"

  // Location config
  client_region           = module.info.regions.us_a
  server_region           = module.info.regions.us_a
  cloudsql_region         = module.info.regions.us_a
  storage_bucket_location = module.info.regions.us_a
  bucket_cai_location     = module.info.regions.us_a

  //Connect to shared VPC
  network         = module.network.state.network_name
  subnetwork      = module.network.state.subnets_names[0]
  network_project = module.network_project.state.project_id
  server_tags     = local.forseti_client_tags
  client_tags     = local.forseti_client_tags

  //Use only config_validator
  config_validator_enabled           = true
  policy_library_sync_enabled        = true
  audit_logging_enabled              = false
  bigquery_enabled                   = false
  blacklist_enabled                  = false
  bucket_acl_enabled                 = false
  cloudsql_acl_enabled               = false
  enabled_apis_enabled               = false
  firewall_rule_enabled              = false
  forwarding_rule_enabled            = false
  group_enabled                      = false
  groups_settings_enabled            = false
  iam_policy_enabled                 = false
  iap_enabled                        = false
  instance_network_interface_enabled = false
  ke_scanner_enabled                 = false
  ke_version_scanner_enabled         = false
  kms_scanner_enabled                = false
  lien_enabled                       = false
  location_enabled                   = false
  log_sink_enabled                   = false
  resource_enabled                   = false
  rules_path                         = false
  service_account_key_enabled        = false
}

resource "google_compute_route" "default" {
  count            = var.cloud_nat_enabled ? 1 : 0
  name             = "${module.forseti_project.state.project_id}-external-route"
  dest_range       = "0.0.0.0/0"
  project          = module.network_project.state.project_id
  network          = module.network.state.network_name
  priority         = 1001
  tags             = concat(local.forseti_client_tags, local.forseti_client_tags)
  next_hop_gateway = "default-internet-gateway"
}

resource "google_compute_router" "forseti_router" {
  count   = var.cloud_nat_enabled ? 1 : 0
  name    = "gcp-forseti-${var.env}-router"
  project = module.network_project.state.project_id
  region  = module.info.regions.us_a
  network = module.network.state.network_name

  bgp {
    asn            = 64514
    advertise_mode = "CUSTOM"
    advertised_ip_ranges {
      range = "${module.forseti.forseti-server-vm-ip}/32"
    }
    advertised_ip_ranges {
      range = "${module.forseti.forseti-client-vm-ip}/32"
    }
  }
}

resource "google_compute_router_nat" "forseti_nat" {
  count                              = var.cloud_nat_enabled ? 1 : 0
  project                            = module.network_project.state.project_id
  region                             = module.info.regions.us_a
  name                               = "gcp-forseti-${var.env}-nat"
  router                             = google_compute_router.forseti_router[0].name
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}
