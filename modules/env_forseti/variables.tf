# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

variable "env" {
  description = "The environment in which the forseti should be created (dev, tst, prod)"
  type        = string
}

variable "cloud_nat_enabled" {
  description = "Used to toggle Cloud NAT"
  type        = bool
  default     = true
}
