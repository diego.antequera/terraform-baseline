# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../modules/info"
}

module "project" {
  source = "../../modules/remote"

  component = "projects"
  env       = "${var.env}_security"
}

module "kms" {
  source  = "terraform-google-modules/kms/google"
  version = "~> 1.1.0"

  project_id = module.project.state.project_id
  location   = "us"
  keyring    = "${module.info.prefix}-gcp-${var.env}-security-us-central-kring"
  owners     = module.info.iam_groups["security_admin"]
}