# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "keyring" {
  description = "Self link of the keyring."
  value       = module.kms.keyring 
}

output "keyring_name" {
  description = "Name of the keyring."
  value       = module.kms.keyring 
}

output "keyring_resource" {
  description = "Keyring resource"
  value       = module.kms.keyring 
}	