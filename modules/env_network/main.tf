# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../modules/info"
}

module "folders" {
  source = "../../modules/remote"

  component = "folders"
  env       = "core"
}

module "project" {
  source = "../../modules/remote"

  component = "projects"
  env       = "${var.env}_network"
}

module "transit_network" {
  source = "../../modules/remote"

  component = "network"
  env       = "transit"
}

module "shared_vpc" {
  source = "../../modules/network"

  project_id           = module.project.state.project_id
  env                  = var.env
  cidr_block           = lookup(module.info.cidr_block, var.env, null)
  regions              = module.info.regions
  blk_ranges           = module.info.blk_ranges
  monitoring_ranges    = module.info.monitoring_ranges
  tcp_monitoring_ports = module.info.tcp_monitoring_ports
  udp_monitoring_ports = module.info.udp_monitoring_ports
  flow_logs            = var.env == "prod" ? true : false
}

module "transit_peering" {
  source  = "terraform-google-modules/network/google//modules/network-peering"
  version = "~> 2.0"

  local_network              = module.shared_vpc.network_self_link
  peer_network               = module.transit_network.state.network_self_link
  module_depends_on          = [module.shared_vpc]
  export_local_custom_routes = true
  export_peer_custom_routes  = true
}
