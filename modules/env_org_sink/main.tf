# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../modules/info"
}

module "folders_root" {
  source    = "../../modules/remote"
  component = "folders"
  env       = "root"
}

module "security_sink_project" {
  source    = "../../modules/remote"
  component = "projects"
  env       = "${var.env}_security_org_sink"
}

locals {
  parent_folder_id = (var.env == "prod"
    ? module.folders_root.state.redacted_folder_id
    : module.folders_root.state["root_${var.env}_folders_id"]
  )
  project_id = module.security_sink_project.state.project_id
}

# ***********************************
# PUBSUB LOG EXPORT
# ***********************************

module "pubsub_logs_export" {
  source                 = "../../modules/external/log_export"
  destination_uri        = module.pubsub_destination.destination_uri
  log_sink_name          = "${module.info.prefix}-${var.env}-security-splunk-log-sink"
  parent_resource_id     = local.parent_folder_id
  parent_resource_type   = "folder"
  unique_writer_identity = false
  include_children       = true
}

module "pubsub_destination" {
  source                   = "../../modules/external/log_export/modules/pubsub"
  project_id               = module.security_sink_project.state.project_id
  log_sink_writer_identity = "${module.pubsub_logs_export.writer_identity}"

  create_subscriber     = true
  topic_name            = "${module.info.prefix}-${var.env}-org-sink-tpc"
  subscriber_account_id = "${module.info.prefix}-${var.env}-org-sink-subscriber"
}

# ***********************************
# DATA ACCESS LOG EXPORT
# ***********************************

module "storage_destination" {
  source                   = "../../modules/external/log_export/modules/storage"
  project_id               = module.security_sink_project.state.project_id
  log_sink_writer_identity = "${module.data_access_exports.writer_identity}"
  storage_bucket_name      = "${module.info.prefix}-${var.env}-security-data-access-sink"
}

module "data_access_exports" {
  source                 = "../../modules/external/log_export"
  destination_uri        = "${module.storage_destination.destination_uri}"
  log_sink_name          = "${module.info.prefix}-${var.env}-security-access-log-sink"
  parent_resource_id     = local.parent_folder_id
  filter                 = "logName:data_access"
  parent_resource_type   = "folder"
  include_children       = true
  unique_writer_identity = true
}

# ***********************************
# PUBSUB CONSUMER
# ***********************************

resource "google_project_iam_custom_role" "log-export-consumer-role" {
  project     = local.project_id
  role_id     = "LogExportConsumerRole"
  title       = "Log Export Consumer Role"
  description = "A custom role to be used with the Splunk add-on for GCP"
  stage       = "GA"

  permissions = [
    "pubsub.subscriptions.list",
    "pubsub.subscriptions.consume",
    "pubsub.subscriptions.update",
    "resourcemanager.projects.get",
  ]
}

resource "google_project_iam_member" "log-export-consumer" {
  project = local.project_id
  role    = "projects/${local.project_id}/roles/${google_project_iam_custom_role.log-export-consumer-role.role_id}"
  member  = "serviceAccount:${module.pubsub_destination.pubsub_subscriber}"
}
