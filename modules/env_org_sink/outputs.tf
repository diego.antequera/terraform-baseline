# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "storage_writer_identity" {
  description = "The service account that logging uses to write log entries to the storage destination."
  value       = module.data_access_exports.writer_identity
}

output "pubsub_writer_identity" {
  description = "The service account that logging uses to write log entries to the destination."
  value       = module.pubsub_logs_export.writer_identity
}

output "pubsub_subscriber" {
  description = "The service account has subscribe access to the export pubsub"
  value       = module.pubsub_destination.pubsub_subscriber
}

output "destination_uri" {
  description = "Pubsub URI created"
  value       = module.pubsub_destination.destination_uri
}
