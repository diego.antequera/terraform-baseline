# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

locals {
  dmz_tag      = ["gcp-blk-dmz-sa"]
  jump_tag     = ["gcp-blk-jump-sa"]
  internal_tag = ["gcp-blk-internal-sa"]
  all_range    = ["0.0.0.0/0"]
  iap_range    = ["35.235.240.0/20"]

  ssh_rdp_ports = ["22", "3389"]
  http_s_ports  = ["80", "443"]

  commom_firewall_rules = {
    gcp-allow-all-http-https-ingress = {
      prefix               = ""
      description          = "Allow access to 80 and 443 for all the network"
      direction            = "INGRESS"
      action               = "allow"
      ranges               = local.all_range
      use_service_accounts = false
      sources              = null
      targets              = null
      rules = [
        {
          protocol = "TCP"
          ports    = local.http_s_ports
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
    gcp-allow-all-ssh-rdp-ingress = {
      prefix               = ""
      description          = "Allow access to 22 and 3389 for all the network"
      direction            = "INGRESS"
      action               = "allow"
      ranges               = local.all_range
      use_service_accounts = false
      sources              = null
      targets              = null
      rules = [
        {
          protocol = "TCP"
          ports    = local.ssh_rdp_ports
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
  }

  sb_firewall_rules = {
    gcp-ssh-rdp-allow-ingress-iap-vpc = {
      prefix               = ""
      description          = "Allow GCP Identity Aware Proxy access"
      direction            = "INGRESS"
      action               = "allow"
      ranges               = local.iap_range
      use_service_accounts = false
      sources              = null
      targets              = null
      rules = [
        {
          protocol = "TCP"
          ports    = local.ssh_rdp_ports
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
  }

  prod_firewall_rules = {
    // Override the denny all ingress for logging
    gcp-override-deny-ingress = {
      prefix               = ""
      description          = "Denny all ingress"
      direction            = "INGRESS"
      action               = "deny"
      ranges               = local.all_range
      use_service_accounts = false
      sources              = null
      targets              = null
      rules = [
        {
          protocol = "ALL"
          ports    = []
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }

    gcp-override-deny-egress = {
      prefix               = ""
      description          = "Deny all egress"
      direction            = "EGRESS"
      action               = "deny"
      ranges               = local.all_range
      use_service_accounts = false
      sources              = null
      targets              = null
      rules = [
        {
          protocol = "ALL"
          ports    = []
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }

    gcp-trafic-allow-egress-all-to-monitoring = {
      prefix               = ""
      description          = "Allow egress to monitoring agents"
      direction            = "EGRESS"
      action               = "allow"
      ranges               = var.monitoring_ranges
      use_service_accounts = false
      sources              = null
      targets              = null
      rules = [
        {
          protocol = "TCP"
          ports    = var.tcp_monitoring_ports
        },
        {
          protocol = "UDP"
          ports    = var.udp_monitoring_ports
        }
      ]

      extra_attributes = {
        priority       = 1
        enable_logging = "true"
      }
    }
  }

  env_rules = {
    sb      = merge(local.commom_firewall_rules, var.env == "sb" ? local.sb_firewall_rules : {})
    dev     = local.commom_firewall_rules
    transit = local.commom_firewall_rules
    tst     = local.prod_firewall_rules
    prod    = local.prod_firewall_rules
  }
}

module "vpc-firewall-rules" {
  source = "../external/fabric-net-firewall"

  project_id          = var.project_id
  network             = var.network_name
  custom_rules        = var.shared_vpc_rules ? local.env_rules[var.env] : {}
  ssh_source_ranges   = []
  http_source_ranges  = []
  https_source_ranges = []
}

module "project-firewall-rules" {
  source = "../external/fabric-net-firewall"

  project_id          = var.project_id
  network             = var.network_name
  custom_rules        = var.shared_vpc_rules ? {} : var.custom_rules
  ssh_source_ranges   = []
  http_source_ranges  = []
  https_source_ranges = []
}
