# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

variable "project_id" {
  description = "The project id where the firewall rules must be added"
  type        = string
}

variable "network_name" {
  description = "The name where the firewall rules must be added"
  type        = string
}

variable "env" {
  description = "The environment of the project (prod, nonprod)"
  type        = string
  default     = "dev"
}

variable "blk_ranges" {
  description = "The CIDR blocks that belong to redacted"
  type        = list(string)
  default     = []
}

variable "monitoring_ranges" {
  description = "The monitoring tools IP ranges"
  type        = list(string)
  default     = []
}

variable "tcp_monitoring_ports" {
  description = "The monitoring tools TCP ports"
  type        = list(string)
  default     = []
}

variable "udp_monitoring_ports" {
  description = "The monitoring tools UDP ports"
  type        = list(string)
  default     = []
}

variable "shared_vpc_rules" {
  description = "Used to inform if it should configure the shared vpc rules (use only when creating a shared vpc)"
  default     = false
  type        = bool
}

variable "custom_rules" {
  description = "The custom rules to be added, if informed will override the environment default rules."
  default     = {}
  type = map(object({
    prefix               = string
    description          = string
    direction            = string
    action               = string # (allow|deny)
    ranges               = list(string)
    sources              = list(string)
    targets              = list(string)
    use_service_accounts = bool
    rules = list(object({
      protocol = string
      ports    = list(string)
    }))
    extra_attributes = map(string)
  }))
}
