# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

data "google_organization" "primary" {
  organization = "organizations/${local.org_id}"
}

locals {
  org_id       = "redacted"
  gsuite_admin = "gsuite-export-test@redacted.com"

  billing_account         = "redacted"
  budget_alert_amount     = "100000"
  budget_alert_percentage = 1.0

  cost_centers = {
    cloud = "CLOUD"
  }

  prefix = "blkt"

  environments = [
    "dev",
    "tst",
    "prod",
    "sb"
  ]

  tfstate_bucket = "blkt-shared-core-tfstate-redacted"

  vm_allowed_external_ip = []

  csr_username = "csr_username"

  csr_ssh_key = "csr_ssh_key"

  allowed_domains = ["google.com"]

  splunk_ingestion_pubsub_subscriptions = ["pubsub"]

  regions = {
    us_a = "us-east1"
    us_b = "us-west2"
  }

  zones = {
    us_a = "${local.regions.us_a}-b"
    us_b = "${local.regions.us_b}-b"
  }

  cidr_block = {
    prod = {
      primary_a   = "10.248.0.0/19"
      secondary_a = "172.16.0.0/13"
      primary_b   = "10.248.32.0/19"
      secondary_b = "172.24.0.0/13"
    }
    tst = {
      primary_a   = "10.248.64.0/19"
      secondary_a = "172.16.0.0/13"
      primary_b   = "10.248.96.0/19"
      secondary_b = "172.24.0.0/13"
    }
    dev = {
      primary_a   = "10.248.128.0/19"
      secondary_a = "172.16.0.0/13"
      primary_b   = "10.248.160.0/19"
      secondary_b = "172.24.0.0/13"
    }
    transit = {
      primary_a   = "10.248.192.0/21"
      secondary_a = "172.16.0.0/13"
      primary_b   = "10.248.200.0/21"
      secondary_b = "172.24.0.0/13"
    }
  }

  blk_ranges = ["10.0.0.0/8"]

  monitoring_ranges = ["10.0.0.0/8"]

  udp_monitoring_ports = ["8080"]

  tcp_monitoring_ports = ["80"]

  tcp_application_ports = ["8080"]

  allow_internal_egress_ports = ["80", "8080"]

  forseti_domain = "redacted.com"

  forseti_gsuite_admin_email = "admin@redacted.com"

  gcp_base_domain = "gcp.redacted.com"

  iam_groups = {
    billing_admin    = ["billing-admins@v"]
    billing_user     = ["billing-users@redacted.com"]
    network_admin    = ["network-admins@redacted.com"]
    org_admin        = ["project-team@redacted.com", "org-admins@redacted.com"]
    os_image_admin   = ["os-image-admins@redacted.com"]
    platform_admin   = ["platform-admins@redacted.com"]
    project_team     = ["project-team@redacted.com"]
    security_admin   = ["security-admins@redacted.com"]
    security_auditor = ["security-auditors@redacted.com"]
  }
  project_activate_apis = ["bigquery.googleapis.com",
    "bigquerystorage.googleapis.com",
    "bigquerydatatransfer.googleapis.com",
    "bigtable.googleapis.com",
    "sqladmin.googleapis.com",
    "redis.googleapis.com",
    "spanner.googleapis.com",
    "storage-api.googleapis.com",
    "storagetransfer.googleapis.com",
    "storage-component.googleapis.com",
    "cloudfunctions.googleapis.com",
    "run.googleapis.com",
    "compute.googleapis.com",
    "dns.googleapis.com",
    "container.googleapis.com",
    "containerscanning.googleapis.com",
    "containerregistry.googleapis.com",
    "containeranalysis.googleapis.com",
    "composer.googleapis.com",
    "datacatalog.googleapis.com",
    "dlp.googleapis.com",
    "dataflow.googleapis.com",
    "dataproc.googleapis.com",
    "pubsub.googleapis.com",
    "binaryauthorization.googleapis.com",
    "cloudbuild.googleapis.com",
    "cloudkms.googleapis.com",
    "iamcredentials.googleapis.com",
    "iam.googleapis.com",
    "automl.googleapis.com",
    "translate.googleapis.com",
    "cloudscheduler.googleapis.com",
    "clouddebugger.googleapis.com",
    "cloudprofiler.googleapis.com",
    "cloudtrace.googleapis.com",
    "logging.googleapis.com",
    "monitoring.googleapis.com",
    "cloudmonitoring.googleapis.com",
    "serviceusage.googleapis.com",
    "cloudbilling.googleapis.com",
    "billingbudgets.googleapis.com",
    "admin.googleapis.com"

  ]

  key_rotation_period       = "5184000s"
  dome9_organizational_unit = "redacted"
}
