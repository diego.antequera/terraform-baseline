# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "org_id" {
  description = "Organization ID to target"
  value       = local.org_id
}

output "gsuite_admin" {
  description = "GSuite Admin email"
  value       = local.gsuite_admin
}

output "billing_account" {
  description = "Main Billing Account ID to use"
  value       = local.billing_account
}

output "cost_centers" {
  description = "Common cost center IDs"
  value       = local.cost_centers
}

output "budget_alert_amount" {
  description = "Amount in USD to be considered as budget"
  value       = local.budget_alert_amount
}

output "budget_alert_percentage" {
  description = "Percentage threshold of when the alert should be thrown"
  value       = local.budget_alert_percentage
}

output "prefix" {
  description = "Prefix to apply for project IDs"
  value       = local.prefix
}

output "environments" {
  description = "GCP environments to provision"
  value       = local.environments
}

output "tfstate_bucket" {
  description = "The bucket used for the tf state"
  value       = local.tfstate_bucket
}

output "vm_allowed_external_ip" {
  description = "List of VMs that are allowed to have external IP"
  value       = local.vm_allowed_external_ip
}

output "allowed_domains" {
  description = "List of allowed domains for the environment"
  value       = local.allowed_domains
}

output "splunk_ingestion_pubsub_subscriptions" {
  description = "Pub/Sub Subscriptions that the Splunk Ingestion account should have"
  value       = local.splunk_ingestion_pubsub_subscriptions
}

output "regions" {
  description = "Regions used to deploy services"
  value       = local.regions
}

output "zones" {
  description = "Zones used to deploy services"
  value       = local.zones
}

output "cidr_block" {
  description = "CIDR Blocks used for the network"
  value       = local.cidr_block
}

output "blk_ranges" {
  description = "The CIDR blocks that belong to redacted on-prem environment"
  value       = local.blk_ranges
}

output "monitoring_ranges" {
  description = "The monitoring tools IP ranges"
  value       = local.monitoring_ranges
}

output "udp_monitoring_ports" {
  description = "The monitoring tools UDP ports"
  value       = local.udp_monitoring_ports
}

output "tcp_monitoring_ports" {
  description = "The monitoring tools TCP ports"
  value       = local.tcp_monitoring_ports
}

output "tcp_application_ports" {
  description = "The applications TCP ports"
  value       = local.tcp_application_ports
}

output "allow_internal_egress_ports" {
  description = "The ports that internal SA machines are able to egress trafic to."
  value       = local.allow_internal_egress_ports
}

output "forseti_domain" {
  description = "The Forseti domain"
  value       = local.forseti_domain
}

output "forseti_gsuite_admin_email" {
  description = "The forseti admin email"
  value       = local.forseti_gsuite_admin_email
}

output "org_domain" {
  description = "The main organization domain"
  value       = data.google_organization.primary.domain
}

output "gcp_base_domain" {
  description = "The GCP Base domain to be used for DNS"
  value       = local.gcp_base_domain
}

output "iam_groups" {
  description = "IAM Groups Ids"
  value       = local.iam_groups
}

output "project_activate_apis" {
  description = "API that should be enabled in a project"
  value       = local.project_activate_apis
}

output "key_rotation_period" {
  description = "KMS Key rotation period"
  value       = local.key_rotation_period
}

output "dome9_organizational_unit" {
  description = "Dome9 organizational Unit ID"
  value       = local.dome9_organizational_unit
}

output "csr_username" {
  description = "CSR Username"
  value       = local.csr_username
}

output "csr_ssh_key" {
  description = "CSR SSH Key"
  value       = local.csr_ssh_key
}

