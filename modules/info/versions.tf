# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

terraform {
  required_version = "~> 0.12.19"
}

provider "google" {
  version = "~> 3.5"
}

provider "google-beta" {
  version = "~> 3.5"
}

provider "null" {
  version = "~> 2.1"
}

provider "random" {
  version = "~> 2.2"
}
