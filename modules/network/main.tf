# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "shared_vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 2.0.2"

  project_id                             = var.project_id
  network_name                           = "gcp-${var.env}-vpc"
  routing_mode                           = "GLOBAL"
  delete_default_internet_gateway_routes = true
  description                            = "This is the ${var.env} VPC."
  shared_vpc_host                        = true

  subnets = [
    {
      subnet_name               = "gcp-${var.env}-vpc-${var.regions.us_a}-global"
      subnet_ip                 = var.cidr_block.primary_a
      subnet_region             = var.regions.us_a
      subnet_private_access     = "true"
      subnet_flow_logs          = var.flow_logs
      subnet_flow_logs_interval = "INTERVAL_5_SEC"
      subnet_flow_logs_sampling = 1.0
      subnet_flow_logs_metadata = "INCLUDE_ALL_METADATA"
      description               = "This is the subnet for the region ${var.regions.us_a}"
    },
    {
      subnet_name               = "gcp-${var.env}-vpc-${var.regions.us_b}-global"
      subnet_ip                 = var.cidr_block.primary_b
      subnet_region             = var.regions.us_b
      subnet_private_access     = "true"
      subnet_flow_logs          = var.flow_logs
      subnet_flow_logs_interval = "INTERVAL_5_SEC"
      subnet_flow_logs_sampling = 1.0
      subnet_flow_logs_metadata = "INCLUDE_ALL_METADATA"
      description               = "This is the subnet for the region ${var.regions.us_b}"
    }
  ]

  secondary_ranges = var.env != "" ? {} : {
    "gcp-${var.env}-vpc-${var.regions.us_a}-global" = [
      {
        range_name    = "gcp-${var.env}-${var.regions.us_a}-sr"
        ip_cidr_range = var.cidr_block.secondary_a
      }
    ]
    "gcp-${var.env}-vpc-${var.regions.us_b}-global" = [
      {
        range_name    = "gcp-${var.env}-${var.regions.us_b}-sr"
        ip_cidr_range = var.cidr_block.secondary_b
      }
    ]
  }
}

module "firewall_rules" {
  source = "../firewall"

  project_id           = var.project_id
  network_name         = module.shared_vpc.network_name
  env                  = var.env
  blk_ranges           = var.blk_ranges
  shared_vpc_rules     = true
  monitoring_ranges    = var.monitoring_ranges
  tcp_monitoring_ports = var.tcp_monitoring_ports
  udp_monitoring_ports = var.udp_monitoring_ports
}
