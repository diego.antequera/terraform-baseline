# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

variable "project_id" {
  description = "The project id where the project must be added"
  type        = string
}

variable "env" {
  description = "The environment in witch this vpc should be inserted (dev, tst, prod)"
  type        = string
}

variable "cidr_block" {
  description = "The CIDR blocks that will be used, for primary and secondary"
  type        = map
}

variable "regions" {
  description = "Region A and B where the subnets should be created."
  type        = map
}

variable "blk_ranges" {
  description = "The CIDR blocks that belong to redacted"
  type        = list(string)
}

variable "monitoring_ranges" {
  description = "The monitoring tools IP ranges"
  type        = list(string)
}

variable "tcp_monitoring_ports" {
  description = "The monitoring tools TCP ports"
  type        = list(string)
}

variable "udp_monitoring_ports" {
  description = "The monitoring tools UDP ports"
  type        = list(string)
}

variable "flow_logs" {
  description = "Used to indicate the need to activate flow logs"
  type        = bool
  default     = false
}