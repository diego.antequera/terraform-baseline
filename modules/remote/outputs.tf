# Copyright 2019 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

output "state" {
  description = "Remote state data"
  value       = data.terraform_remote_state.state.outputs
}
