# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

variable "component" {
  description = "The component to retrieve remote state for"
  type        = string
}

variable "env" {
  description = "The environment to retrieve remote state for"
  type        = string
}
