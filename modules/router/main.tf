# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

module "info" {
  source = "../../modules/info"
}

locals {
  names = {
    for region in keys(module.info.regions) :
    region => "gcp-transit-router-${module.info.regions[region]}"
  }
}

module "router" {
  source  = "terraform-google-modules/cloud-router/google"
  version = "~> 0.1"

  project = var.project_id
  network = var.network

  name   = local.names[var.region]
  region = module.info.regions[var.region]

  bgp = {
    # 16550 required for Partner Interconnect
    asn               = 16550
    advertised_groups = ["ALL_SUBNETS"]
  }
}

module "router-secondary" {
  source  = "terraform-google-modules/cloud-router/google"
  version = "~> 0.1"

  project = var.project_id
  network = var.network

  name   = "${local.names[var.region]}-b"
  region = module.info.regions[var.region]

  bgp = {
    # 16550 required for Partner Interconnect
    asn               = 16550
    advertised_groups = ["ALL_SUBNETS"]
  }
}
