# Copyright 2020 Google LLC. This software is provided as-is, without warranty or representation for any use or purpose.
# Your use of it is subject to your agreement with Google.

variable "project_id" {
  description = "The project ID to create routers in"
  type        = string
}

variable "network" {
  description = "The network name to attach routers to"
  type        = string
}

variable "region" {
  description = "The region identifier to create a router for"
  type        = string
}
